
#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <GL/glut.h>

#include "src/ilumination.h"
#include "src/draw.h"
#include "src/loadTextures.h"
#include "src/loaderModels.h"
#include "src/Camera.h"
#include "src/hud.h"
#include "src/particles.h"
#include "src/drawExtra.h"
#include "src/detailsExtras.h"

#define BLACK 0.0f, 0.0f, 0.0f, 1.0f
#define MAP 1
/*
extern float rotates[], translates[];
extern float x_bola,x_inicial,y_bola,y_inicial, time, velocity, bounce, MAX_ALT, radiusCone, hCone;
extern bool CAN_NOT_EMPTY,drawCone;
extern int Colision;
*/
int lightsEnabled = 1;
// MOUSE VARS
int prev_x = 800/2, prev_y = 600/2;

int keyPressed;		// var global partilhada. Permite saber que tecla 'E' foi premida
int angulo = 10;	

int zoom=88.0;

extern int sitDown;
extern float corCopo[];

GLint     wScreen=800, hScreen=600;
//------------------------------------------------------------ Observador
CCamera Camera;

GLint    msec=35;					//.. definicao do timer (actualizacao)

//------------------------------------------------------------ Texturas
GLuint  texture[11];
GLuint	textureExtra[1];
//------------------------------------------------------------ Modelos
Objeto models[5];
Objeto modelsExtra[1];


void resizeWindow(GLsizei w, GLsizei h)
{
 	wScreen=w;
	hScreen=h;
	//glViewport( 0, 0, wScreen,hScreen );	
	//glutReshapeWindow(wScreen,hScreen);
	glutPostRedisplay();
}

void display(void){
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~[ Apagar ]
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	
	glViewport(0,0,wScreen,hScreen);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(zoom, wScreen/hScreen, 0.1, 100);
	/*gluPerspective(88.0, wScreen/hScreen, 0.1, 100);*/
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	Camera.Render();
	
	drawExtras(modelsExtra, textureExtra);
	drawScene(texture);
	draw3DModels(models, texture);
	
	
	/* DESENHO MAPA */
	glViewport (0, 0, 100, 100);	// viewport para borda do mapa
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-5, 5, -5, 5, 1, 10000);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();
		gluLookAt(Camera.getPositionX(), 10, Camera.getPositionZ(), Camera.getPositionX(), 0, Camera.getPositionZ(), 0 , 0, -1);
		// mover camera para posicao da camera, a altura 10 a olhar para baixo
		glTranslatef(Camera.getPositionX(), 0, Camera.getPositionZ());
		//glRotatef(180, 0, 0, 1);
		//glRotatef(90, 1, 0, 0);
		glRotatef(180+angulo, 0, 1, 0);
		glTranslatef(-Camera.getPositionX(), 0, -Camera.getPositionZ());

		drawBorders(MAP);
		glViewport (5, 5, 90, 90); // viewport para cena. Assim a borda do mapa (plano preto) mantem-se
		/*
		drawExtras(modelsExtra, textureExtra);
		drawScene(texture);
		draw3DModels(models, texture);
		*/
	glPopMatrix();

	glViewport (5, 5, 90, 90); // viewport para cena. Assim a borda do mapa (plano preto) mantem-se
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-5, 5, -5, 5, 1, 10000);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();
		gluLookAt(Camera.getPositionX(), 10, Camera.getPositionZ(), Camera.getPositionX(), 0, Camera.getPositionZ(), 0 , 0, -1);
		// mover camera para posicao da camera, a altura 10 a olhar para baixo
		glTranslatef(Camera.getPositionX(), 0, Camera.getPositionZ());
		//glRotatef(180, 0, 0, 1);
		//glRotatef(90, 1, 0, 0);
		glRotatef(180+angulo, 0, 1, 0);
		glTranslatef(-Camera.getPositionX(), 0, -Camera.getPositionZ());

		drawExtras(modelsExtra, textureExtra);
		drawScene(texture);
		draw3DModels(models, texture);
	
	glPopMatrix();


	drawHUD(Camera.getPositionX(), Camera.getPositionZ()); // caso camera esteja perto da bola apresenta HUD
	

	//finish rendering
	glutSwapBuffers();
}


void Timer(int value) 
{
	glutPostRedisplay();
	glutTimerFunc(msec,Timer, 1);
}

void zoomer(float input){

	if(input > 25.0 && input < 90.0){									// antes || e agora &&
		zoom=input;
	}
}

void shader(float input){

	if( (input<0.0 && corCopo[3] + input > 0.19) || (input > 0.0 && input + corCopo[3]< 0.9) ){									// antes || e agora &&
		corCopo[3]=corCopo[3]+ input;
	}
}
/* //Em src/draw.c
void resetScene(){
	keyPressed = 0;
	Colision = 0;
	time = 0.0f;
	velocity = 40.0f;
	bounce = -1.0;
	MAX_ALT = 7.0f;
	CAN_NOT_EMPTY = true;
	drawCone = false;
	
	radiusCone = 0.3f;
	hCone = 0.1f;

	// RESET LATA 
	rotates[0] = 0.0f;	
	translates[0] = 19.5f;
	translates[1] = 4.3f;
	translates[2] = 15.0f;
	
	// RESET BOLA
	x_bola = 10.0f;
	x_inicial = 10.0f;
	y_bola = 4.2f;
	y_inicial = 5.5f;	
}*/

//======================================================= EVENTOS
void keyboard(unsigned char key, int x, int y){
	//printf("Keypressed = %c\n", key);
	switch (key) {
		
		case 'a':
		case 'A':
			if ( sitDown == 0)
				Camera.StrafeRight( -1.0 );
			break;
		case 'd':
		case 'D':
			if ( sitDown == 0)
				Camera.StrafeRight( 1.0 );
			break;
		case 's':
		case 'S':
			if ( sitDown == 0)
				Camera.MoveForward( 1.0 );
			break;
		case 'w':
		case 'W':
			if ( sitDown == 0)
				Camera.MoveForward( -1.0 ) ;
			break;
		
		case 'e': // Apenas aceita input caso esteja perto da bola. Funciona apenas uma vez a "animacao"
		case 'E':
			// interacao com bolo
				// quarto movido -15f em x daí o +15f									     
			if ( (fabs( Camera.getPositionX() +15.0f - x_bola )<= 3.0f) && (fabs( Camera.getPositionZ() - 0  )<= 3.0f) )
				keyPressed = 1;

			// interacao com sofa
			if (  ( ( Camera.getPositionX() + 15.0f >= 10.0f ) && ( Camera.getPositionX() +15.0f <= 21.0f )  ) &&  
				  ( ( Camera.getPositionZ() + 15.0f <= 4 ) ) 											    	)
			{
				sitDown = !sitDown;
				if ( angulo != 0){
					Camera.RotateY(angulo);
				}
				angulo = 0;
				//Camera.setView(0,0,1);
			}
			break;
		
		case 'i':
		case 'I':
			zoomer(zoom-2.0);
			break;

		case 'o':
		case 'O':
			zoomer(zoom+2.0);
			break;


		case 'l':
		case 'L':
			shader(-0.1);
			break;

		case 'K':
		case 'k':
			shader(0.1);
			break;

		case 'r':
		case 'R':
			/*printf("%f %f %f %f\n", Camera.getPositionX(), translates[0], Camera.getPositionZ(),  translates[1]);
			
			printf("%d\n",fabs(Camera.getPositionX() - translates[0]) <= 2 && fabs(Camera.getPositionZ()) <= 2);
			*/									// 20.0f <=> pos Lata em x
			if ( fabs(Camera.getPositionX()+15.0f - 19.5f) <= 2.0f && fabs(Camera.getPositionZ()) <= 2.0f )
			{
				//printf("RESET\n");
				resetScene();
			}
			break;

		/*
		case 'q':
		case 'Q':
			if ( lightsEnabled )
				glDisable(GL_LIGHTING);  
			else
				glEnable(GL_LIGHTING);
			lightsEnabled = !lightsEnabled;
			//printf("%d\n", lightsEnabled);
			break;*/

		//--------------------------- Escape
		case 27:
			exit(0);
			break;	
	}
	
	//Camera.print();

	glutPostRedisplay();
}

void teclasNotAscii(int key, int x, int y){
    //printf("Keypressed = %d\n", key);
	if(key == GLUT_KEY_UP && sitDown == 0)
		Camera.MoveForward( -1.0 ) ;
	if(key == GLUT_KEY_DOWN && sitDown == 0) 
		Camera.MoveForward( 1.0 );
	if(key == GLUT_KEY_LEFT && sitDown == 0)
	{	
		Camera.StrafeRight( -1.0 );
	}
	if(key == GLUT_KEY_RIGHT && sitDown == 0) 
	{
		Camera.StrafeRight( 1.0 );
	}
	//Camera.print();
	
	glutPostRedisplay();
}

void clean(){
	freeObjs(models);
	freeObjs(modelsExtra);
	printf("All cleaned.\n");
}

void init(void)
{
	glClearColor(BLACK);
	glShadeModel(GL_SMOOTH);
	
	loadTextures(texture);
	printf("Todas as texturas foram carregadas com sucesso!\n");
	loadObjs(models);
	printf("Todos os modelos foram carregados com sucesso!\n");

	
	loadTexturesExtra(textureExtra);
	printf("Todas as texturas extra foram carregadas com sucesso!\n");
	loadObjsExtra(modelsExtra);
	printf("Todos os modelosExtra foram carregados com sucesso!\n");
	

	//generateCracks();
	initLigths();

	initParticles();
	printf("Particulas inicializadas.\n");

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	
	keyPressed = 0;
	// este dois rotates cervem para que a camera determine o ViewPoint de forma a poder desde o inicio do programa
	// desenhar a cena.
	Camera.RotateY( 5.0 );
	Camera.RotateY( -5.0 ); 
}

void mouseFunc(int x, int y){
	int deltaX = prev_x - x;
	int deltaY = prev_y - y;


	if ( abs(deltaX) > 1 ){
		//printf("Angulo %d deltaX = %d\n", angulo, deltaX);
		if ( sitDown ){
			if ( angulo < -90 && deltaX > 0 )
				deltaX = 0;
			if ( angulo > 90 && deltaX < 0)
				deltaX = 0; 
		}
		Camera.RotateY( deltaX/5 );
		angulo -= deltaX/5;
		
		prev_x = x;
	
	}

	if ( abs(deltaY) > 1 ){
		Camera.RotateX( deltaY/5 );
		prev_y = y;
	}

	/*
		Seguintes verificaçoes servem para que seja possivel o rodar o rato para a esquerda ou direita "infinitamente"
	*/
	if ( prev_x <=100 || prev_x >= 700 )
	{
		prev_x = 400;
		glutWarpPointer( 400 , prev_y );
	}
	if ( prev_y <=100 || prev_y >= 500 )
	{
		prev_y = 300;
		glutWarpPointer( prev_x , 300 );
	}

//	printf("prev_X = %d\nprev_y=%d\n", prev_x, prev_y);

}

void mouse(int button, int state, int x, int y)
{


	//(button == 0) || (button == 1) ||	(button == 2)		<-- butao clique direito , centro esquerda.



   // Wheel reports as button 3(scroll up) and button 4(scroll down)
   

   if ( button > 2) // It's a wheel event
   {
       // Each wheel event reports like a button click, GLUT_DOWN then GLUT_UP
       //if (state == GLUT_UP) return; // Disregard redundant GLUT_UP events
       
	   printf("Scroll %d: %s At %d %d\n", button, (button == 3) ? "Up" : "Down", x, y);
   }else{  // normal button event
       printf("Button %d: %s At %d %d\n", button, (state == GLUT_DOWN) ? "Down" : "Up", x, y);
   }
}



//======================================================= MAIN
int main(int argc, char** argv){

	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
	glutInitWindowSize (wScreen, hScreen); 
	glutInitWindowPosition (-10, 0); 
	glutCreateWindow ("{jpdmartins,prcoito}@student.dei.uc.pt-CG :: andar:{a,s,d,w} camera:{rato} interagir c/ bola: 'e' zoom: 'i','o' blend: 'k','l'");
  
	init();
	
	glutSetCursor(GLUT_CURSOR_NONE);
	glutWarpPointer( 800/2 , 600/2 );	// set mouse para pos x, y;
	glutPassiveMotionFunc(mouseFunc);
	glutSpecialFunc(teclasNotAscii); 
	//glutMouseFunc(mouse);
	glutDisplayFunc(display); 
	glutReshapeFunc(resizeWindow);
	glutKeyboardFunc(keyboard);
	glutTimerFunc(msec, Timer, 1);


	// apos a execucao do programa chama a funcao clean para libertar memoria alocada pelos objetos 3d
	// caso o programa "crashe" a meio a funcao clean nao será chamada
	atexit(clean);

	glutMainLoop();

	return 0;
}

