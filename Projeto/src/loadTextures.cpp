#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <GL/glut.h>
#include "../SOIL/SOIL.h"

#define TOTAL_TEXTURES 			10
#define CAN_TEXTUTE				0
#define LAMP_TEXTURE			1
#define WINDOW_TEXTURE			2
#define TABLE_TEXTURE			3
#define INTERIOR_WALL_TEXTURE 	4
#define FLOOR_TEXTURE 			5
#define ROOF_TEXTURE			6
#define SKYBOX_TEXTURE			7
#define TERRAIN_TEXTURE			8
#define CRACK_TEXTURE			9

const char ListTextures[][30] = {
	"Textures\\Can.jpg",
	"Textures\\Lamp.png",
	"Textures\\Window.jpg",
	"Textures\\Table.png",
	"Textures\\InteriorWall.jpg",
	"Textures\\Floor.jpg",
	"Textures\\Roof.jpg",
	"Textures\\SkyBox.jpg",
	"Textures\\Terrain.jpg",
	"Textures\\Crack.png"
	//"Textures\\Particle.png"
	};

void loadTextures(GLuint *texture)
{   
	char filename[30];
	
	for (int i=0; i<TOTAL_TEXTURES; i++)
	{
		strcpy(filename, ListTextures[i]);
		texture[i] = SOIL_load_OGL_texture(filename, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);
		if (texture[i] == 0)
			printf("ERROR READING TEXTURE %s\n", filename);
		
		glBindTexture(GL_TEXTURE_2D, texture[i]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
	}
	
}