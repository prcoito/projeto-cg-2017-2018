#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "loaderModels.h"

#define TOTAL_MODELS 5

#define DEBUG 2

int numObjetosLoaded = 0;

char loc_models[][30] = {				// localizacao dos modelos a serem carregados
	"3D_Models\\Can.obj",
	"3D_Models\\Lamp.obj",
	"3D_Models\\Window.obj",
	"3D_Models\\Glass.obj",
	"3D_Models\\Table.obj"
};

int parseLine(char *, int *,int *,int *);
void freeObj(Objeto modelo);

/*	
	leitura do ficheiro .obj linha a linha.
	caso a linha seja:
		definicao de vertice: v x y z
		definicao de coordenada de textura: vt s t w  		<= w é ignorado visto que raramente se usa
		definicao de face:	f v_index/vt_index/vn_index 	<= vn_index para já é ignorado.
	é guardado a info no array respetivo:
		vertex na estrutura objeto
		txtPts na estrutura objeto
		vertex e txtPts na estrutura face 		<= aqui sao os indices correspondentes nos 2 array anteriores
	respetivamente.

	A memoria é alocada durante a leitura do ficheiro (caso necessario)
	No fim da leitura é libertada memoria nao utilizada em cada um dos array alocados dinamicamente

*/
Objeto loadObj(char *filename){
	Objeto modelo;
	char line[100];
	FILE *fp = fopen(filename, "r");
	double x,y,z, s, t;

	int arrayPts[4], arrayTxt[4], arrayNrml[4], numAAdicionar;
	int sizeNumVertexAllocated = 3*100 +2, sizeNumTxtPtsAllocated = 2*100 +2, sizeNumNormalsAllocated = 3*100 +2;
	int sizeNumFacesAllocated = 4;
	int total_de_mini_objetos = 1;

	/* temp pointers to check reallocs success/insuccess */
	double *tempDoublePt;
	Face *tempFacePt;
	MiniObjeto *tempMiniObjetoPt;

	if (fp){
		// "Preparacao" do modelo [mallocs iniciais]
		modelo.mObjecto = (MiniObjeto *)malloc(1 * sizeof(MiniObjeto)); 	// Apenas aloco espaco para um porque há casos em que o .obj
															// apenas tem 1 "mini objeto"
		modelo.numObjetos = 0;
		modelo.mObjecto[0].faces = (Face *)malloc(4 * sizeof(Face)); // Alloca espaço para 4 faces.
		for(int i = 0; i< 4; i++)
			modelo.mObjecto[0].faces[i].numVertices = 3;

		modelo.mObjecto[0].numFaces = 0;
		modelo.vertex  = (double *)malloc((3*100 +2) * sizeof(double));			// Alocacao inicial para 100 vertices [x,y,z]  (+2 de "folga")
		modelo.normals = (double *)malloc((3*100 +2) * sizeof(double));			// Alocacao inicial para 100 vetores [x,y,z]  (+2 de "folga")
		modelo.txtPts  = (double *)malloc((2*100 +2) * sizeof(double));			// Alocacao inicial para 100 pontos de textura [s,t]  (+2 de "folga")
		modelo.numVertex  = 0;
		modelo.numTxtPts  = 0;
		modelo.numNormals = 0;

		if (!modelo.mObjecto || !modelo.mObjecto[0].faces || !modelo.vertex || !modelo.txtPts){
			printf("Erro a alocar memoria inicial\n");
		}
		else{
			
			while( fgets(line, 100, fp) ){
				//printf("linha = %s", line);
				if (line[0]=='g'){	// novo mini-objeto. A partir daqui todos os vertices e faces definidos irao pertencer a este mini-objeto
					if (modelo.numObjetos > 1){
						// já foi guardado pelo menos um mini-objeto
						// agora vou libertar memoria que possivelmente nao foi utilizada para
						// guardar faces no mini-objeto anterior
						tempFacePt = (Face *)realloc(modelo.mObjecto[modelo.numObjetos-1].faces, modelo.mObjecto[modelo.numObjetos-1].numFaces * sizeof(Face));
						if ( tempFacePt == NULL ){
							printf("Erro! Nao foi possivel alocar mais memoria.\n");
							freeObj(modelo);
							modelo.numVertex = 0;
							return modelo;
						}
						modelo.mObjecto[modelo.numObjetos-1].faces = tempFacePt;
						tempFacePt = NULL;
					}
					
					modelo.numObjetos++;
					
					if (modelo.numObjetos > total_de_mini_objetos){
						// alocar espaco para mais um mini_objeto com 4 faces pre-alocadas
						total_de_mini_objetos++;
						tempMiniObjetoPt = (MiniObjeto *)realloc(modelo.mObjecto, total_de_mini_objetos * sizeof(MiniObjeto));
						if ( tempMiniObjetoPt == NULL ){
							printf("Erro! Nao foi possivel alocar mais memoria.\n");
							freeObj(modelo);
							return modelo;
						}
						modelo.mObjecto = tempMiniObjetoPt; 
						tempMiniObjetoPt = NULL;
						
						tempFacePt = (Face *)malloc(4 * sizeof(Face)); // Alloca espaço para 4 faces.
						if ( tempFacePt == NULL ){
							printf("Erro! Nao foi possivel alocar mais memoria.\n");
							freeObj(modelo);
							modelo.numVertex = 0;
							return modelo;
						}
						modelo.mObjecto[modelo.numObjetos-1].faces = tempFacePt;
						tempFacePt = NULL;

						for(int i = 0; i< 4; i++)
							modelo.mObjecto[modelo.numObjetos-1].faces[i].numVertices = 0;

						modelo.mObjecto[modelo.numObjetos-1].numFaces = 0;

						sizeNumFacesAllocated = 4;
					}
					
				}
				else if (line[0]=='v' && line[1] !='t') // novo vertice encontrado
				{// exemplo de linha: v x y z
					strtok ( line, " "); // ingnora v;
					
					x = atof(strtok ( NULL, " "));
					y = atof(strtok ( NULL, " "));
					z = atof(strtok ( NULL, " "));
					
					if (modelo.numVertex + 3 > sizeNumVertexAllocated)
					{
						// alocar mais memoria e guardar vertices
						sizeNumVertexAllocated += 3*100 +2;
						tempDoublePt = (double *)realloc(modelo.vertex, sizeNumVertexAllocated * sizeof(double));
						if ( tempDoublePt == NULL ){
							printf("Erro! Nao foi possivel alocar mais memoria.\n");
							freeObj(modelo);
							modelo.numVertex = 0;
							return modelo;
						}
						modelo.vertex = tempDoublePt;
						tempDoublePt = NULL;
						
					}
					modelo.vertex[modelo.numVertex  ] = x;
					modelo.vertex[modelo.numVertex+1] = y;
					modelo.vertex[modelo.numVertex+2] = z;
					modelo.numVertex+=3;
					
				}
				else if (line[0]=='v' && line[1] == 'n') // novo vertice encontrado
				{// exemplo de linha: v x y z
					strtok ( line, " "); // ingnora v;
					
					x = atof(strtok ( NULL, " "));
					y = atof(strtok ( NULL, " "));
					z = atof(strtok ( NULL, " "));
					
					if (modelo.numVertex + 3 > sizeNumNormalsAllocated)
					{
						// alocar mais memoria e guardar vertices
						sizeNumNormalsAllocated += 3*100 +2;
						tempDoublePt = (double *)realloc(modelo.normals, sizeNumNormalsAllocated * sizeof(double));
						if ( tempDoublePt == NULL ){
							printf("Erro! Nao foi possivel alocar mais memoria.\n");
							freeObj(modelo);
							modelo.numVertex = 0;
							return modelo;
						}
						modelo.normals = tempDoublePt;
						tempDoublePt = NULL;

					}
					modelo.normals[modelo.numNormals  ] = x;
					modelo.normals[modelo.numNormals+1] = y;
					modelo.normals[modelo.numNormals+2] = z;
					modelo.normals+=3;
					
				}
				else if (line[0]=='v' && line[1] =='t'){ // && strcmp(filename, "3D_Models\\Can.obj") == 0){ 
					// ponto de textura. Atualmente apenas guarda para a lata
					// exemplo de linha: vt s t w
					
					strtok ( line, " "); // ingnora vt;
					
					s = atof( strtok ( NULL, " "));
					t = atof( strtok ( NULL, " "));
					
					if (modelo.numTxtPts + 2 > sizeNumTxtPtsAllocated)
					{
						// alocar mais memoria e guardar coordenadas
						sizeNumTxtPtsAllocated += 2*100 +2;
						tempDoublePt = (double *)realloc(modelo.txtPts, sizeNumTxtPtsAllocated * sizeof(double));
						if ( tempDoublePt == NULL ){
							printf("Erro! Nao foi possivel alocar mais memoria.\n");
							freeObj(modelo);
							modelo.numVertex = 0;
							return modelo;
						}

						modelo.txtPts = tempDoublePt;
						tempDoublePt = NULL;
					}
					
					modelo.txtPts[modelo.numTxtPts  ] = s;
					modelo.txtPts[modelo.numTxtPts+1] = t;
					modelo.numTxtPts+=2;
					
				}
				else if (line[0]=='f'){
					if (modelo.numObjetos == 0)
						modelo.numObjetos++; 
					// nova face encontrada. Será guardada no ultimo mini-objeto definido
					// os valores guardados serao os indices de (x/y/z) do array de vertices do modelo
					// bem como os indices de (s,t) das coordenadas da textura
					// exemplo de linha: f v_index/vt_index[/vn_index] v_index/vt_index[/vn_index] v_index/vt_index[/vn_index], com index >=1 [/vn_index] opcional
					
					
					numAAdicionar = parseLine(line, arrayPts, arrayTxt, arrayNrml);
					if (modelo.mObjecto[modelo.numObjetos-1].numFaces >= sizeNumFacesAllocated){
						// alocar mais memoria e guardar
						sizeNumFacesAllocated+=4;
						tempFacePt = (Face *)realloc(modelo.mObjecto[modelo.numObjetos-1].faces, sizeNumFacesAllocated * sizeof(Face));
					
						if ( tempFacePt == NULL ){
							printf("Erro! Nao foi possivel alocar mais memoria.\n");
							freeObj(modelo);
							modelo.numVertex = 0;
							return modelo;
						}

						modelo.mObjecto[modelo.numObjetos-1].faces = tempFacePt;
						tempFacePt = NULL;
					}
					
					for ( int i = 0; i < numAAdicionar ; i++){
						modelo.mObjecto[modelo.numObjetos-1].faces[modelo.mObjecto[modelo.numObjetos-1].numFaces].vertex[i] = arrayPts[i]-1;
					}
					
					for ( int i = 0; i < numAAdicionar ; i++){
						modelo.mObjecto[modelo.numObjetos-1].faces[modelo.mObjecto[modelo.numObjetos-1].numFaces].txtPts[i] = arrayTxt[i]-1;
					}
					
					for ( int i = 0; i < numAAdicionar ; i++){
						modelo.mObjecto[modelo.numObjetos-1].faces[modelo.mObjecto[modelo.numObjetos-1].numFaces].normalVec[i] = arrayNrml[i]-1;
					}
					
					modelo.mObjecto[modelo.numObjetos-1].faces[modelo.mObjecto[modelo.numObjetos-1].numFaces].numVertices=numAAdicionar;
					
					modelo.mObjecto[modelo.numObjetos-1].numFaces++;
					
				}
				
			}
			// libertar memoria nao usada no array de vertices e de coordenadas das texturas
			modelo.vertex = (double *)realloc(modelo.vertex, modelo.numVertex * sizeof(double));
			modelo.txtPts = (double *)realloc(modelo.txtPts, modelo.numTxtPts * sizeof(double));
			modelo.normals = (double *)realloc(modelo.normals, modelo.numNormals * sizeof(double));
			// libertar espaco nao usado no array de faces do ultimo mini-objeto [ os mini-objetos anteriores já ficou libertado ]
			modelo.mObjecto[modelo.numObjetos-1].faces = (Face *)realloc(modelo.mObjecto[modelo.numObjetos-1].faces, modelo.mObjecto[modelo.numObjetos-1].numFaces*sizeof(Face));

			numObjetosLoaded++;
		}

		fclose(fp);
	}
	else{
		printf("Erro: nao foi possivel carregar objeto");
	}

	
	
	return modelo;
}

void freeObj(Objeto modelo)
{
	
	for (int i = 0; i<modelo.numObjetos; i++){
		free(modelo.mObjecto[i].faces);
	}
	free(modelo.vertex);
	if ( modelo.numTxtPts > 0)	// visto que nem sempre está com algo guardado
		free(modelo.txtPts);
	free(modelo.mObjecto);
	
	return;
}

void printObj(Objeto modelo){
	for(int i = 0; i< modelo.numObjetos; i++){
		printf("Mini-objeto:\n");
		for (int j = 0; j< modelo.mObjecto[i].numFaces; j++){
			printf("\tFace [%d]: ", modelo.mObjecto[i].faces[j].numVertices);
			for (int k = 0; k< modelo.mObjecto[i].faces[j].numVertices; k++){
				printf("%d ",modelo.mObjecto[i].faces[j].vertex[k]);
				printf("[%d]",modelo.mObjecto[i].faces[j].txtPts[k]);
			}
			printf("\n");
							
		}
	}
}

void loadObjs(Objeto *lista){
	
	char filename[30];
	
	for ( int i = 0; i<TOTAL_MODELS; i++){
		strcpy(filename, loc_models[i]);
		lista[i] = loadObj(filename);
		if ( lista[i].numVertex )						// se nao houve erro, ou seja numVertex > 0
			strcpy(lista[i].filename, filename);
		else
		{
			freeObjs(lista, numObjetosLoaded);
			exit(0);
		}
	}
}

void freeObjs(Objeto *lista, int total){
	
	for ( int i = 0; i<total; i++){
		freeObj(lista[i]);
	}
}

void freeObjs(Objeto *lista){
	
	for ( int i = 0; i<TOTAL_MODELS; i++){
		freeObj(lista[i]);
	}
}


/*
int main(){
	
	Objeto modelo = loadObj("Models/Mesa.obj");
	printf("Objeto carregado com sucesso\n");
	printObj(modelo);
	freeObj(modelo);
	printf("Objeto libertado com sucesso\n");

	return 0;
}
*/

int parseLine(char *line, int *arrayPts, int *arrayTxt, int *arrayNrml){
	int lenLine = strlen(line);
	int numAAdicionar = 3;
	char temp[lenLine];
	int i=2;								// para ignorar f e espaço
	int lenTemp=0;
	
	//char c; 								//to delete

	#if DEBUG >=1
	printf("line: %s\n", line);
	#endif

	while(line[i] != '/'){ 					// primeira parte pt1 index
		temp[lenTemp++] = line[i];
		i++;
	}
	temp[lenTemp] = '\0';
	#if DEBUG >1
	printf("PT1: %s\n", temp);
	#endif
	*(arrayPts +0) = atoi(temp);
	
	i++;
	lenTemp = 0;
	while(line[i] != '/' && line[i] != ' '){	// segunda parte ptxt1 index
		temp[lenTemp++] = line[i];
		i++;
	}
	
	if(lenTemp){								// por ser opcional
		temp[lenTemp] = '\0';
		#if DEBUG >1
		printf("PTXT1: %s\n", temp);
		#endif
		*(arrayTxt +0) = atoi(temp);
	}
	else
		*(arrayTxt +0) = 0;
		
	/*while(line[i] != ' ')						// ignora vn index e vai para proximo "ponto" da face
		i++;
	*/
	i++;
	lenTemp = 0;
	
	#if DEBUG == 3
	printf("LINE[i-1]: %c (%d)\n", line[i-1], line[i-1]);
	printf("LINE[i]: %c (%d)\n", line[i], line[i]);
	#endif
	
	if ( line[i-1] !=' ')
	{
		while(line[i] != ' '){						// terceira parte vn index
			temp[lenTemp++] = line[i];
			i++;
		}
	}
	else
		i--;
	
	if(lenTemp){								// por ser opcional
		temp[lenTemp] = '\0';
		#if DEBUG >1
		printf("NRML1: %s\n", temp);
		#endif
		*(arrayNrml +0) = atoi(temp);
	}
	else
		*(arrayNrml +0) = 0;

	i++;
	lenTemp = 0;
	while(line[i] != '/'){ 						// primeira parte pt2 index
		temp[lenTemp++] = line[i];
		i++;
	}
	temp[lenTemp] = '\0';
	#if DEBUG >1
	printf("PT2 %s\n", temp);
	#endif
	*(arrayPts +1)  = atoi(temp);

	i++;
	lenTemp = 0;
	while(line[i] != '/' && line[i] != ' '){	// segunda parte ptxt2 index
		temp[lenTemp++] = line[i];
		i++;
	}
	
	if(lenTemp){								// por ser opcional
		temp[lenTemp] = '\0';
		#if DEBUG >1
		printf("PTXT2 %s\n", temp);
		#endif
		*(arrayTxt +1) = atoi(temp);
	}
	else	
		*(arrayTxt +1) = 0;

	/*while(line[i] != ' ')						// ignora vn index e vai para proximo "ponto" da face
		i++;
	*/
	i++;
	lenTemp = 0;
	#if DEBUG == 3
	printf("LINE[i-1]: %c (%d)\n", line[i-1], line[i-1]);
	printf("LINE[i]: %c (%d)\n", line[i], line[i]);
	#endif
			
	if ( line[i-1] !=' ')
	{
		while(line[i] != ' '){						// terceira parte vn index
			temp[lenTemp++] = line[i];
			i++;
		}
	}
	else
		i--;
	if(lenTemp){								// por ser opcional
		temp[lenTemp] = '\0';
		#if DEBUG >1
		printf("NRML2: %s\n", temp);
		#endif
		*(arrayNrml +1) = atoi(temp);
	}
	else
		*(arrayNrml +1) = 0;

	i++;
	lenTemp = 0;
	while(line[i] != '/'){						// primeira parte pt3 index
		temp[lenTemp++] = line[i];
		i++;
	}
	temp[lenTemp] = '\0';
	#if DEBUG >1
	printf("PT3 %s\n", temp);
	#endif
	*(arrayPts +2)  = atoi(temp);

	i++;
	lenTemp = 0;
	while(line[i] != '/' && line[i] != ' '){	// segunda parte ptxt3 index
		temp[lenTemp++] = line[i];
		i++;
	}
	
	if(lenTemp){								// por ser opcional
		temp[lenTemp] = '\0';
		#if DEBUG >1
		printf("PTXT3 %s\n", temp);
		#endif
		*(arrayTxt +2) = atoi(temp);
	}
	else
		*(arrayTxt +2) = 0;

	/*while(line[i] != ' ')						// ignora vn index e vai para proximo "ponto" da face
		i++;
	*/
	i++;
	lenTemp = 0;
	
	#if DEBUG == 3
	printf("LINE[i-1]: %c (%d)\n", line[i-1], line[i-1]);
	printf("LINE[i]: %c (%d)\n", line[i], line[i-1]);
	#endif
	
	if ( line[i-1] !=' ')
	{
		while(line[i] != ' ' && i<lenLine){						// terceira parte vn index
			temp[lenTemp++] = line[i];
			i++;
		}
	}
	else
		i--;
	
	if(lenTemp){									// por ser opcional
		temp[lenTemp] = '\0';
		#if DEBUG >1
		printf("NRML3: %s\n", temp);
		#endif
		*(arrayNrml +2) = atoi(temp);
	}
	else
		*(arrayNrml +2) = 0;

	i++;
	lenTemp = 0;
	while(line[i] != '/' && i<lenLine){ 						// primeira parte pt4 index (opcional)
		temp[lenTemp++] = line[i];
		i++;
	}
	if ( i < lenLine)
		numAAdicionar = 4;
	temp[lenTemp] = '\0';
	#if DEBUG >1
	printf("PT4 %s\n", temp);
	#endif
	*(arrayPts +3)  = atoi(temp);

	i++;
	lenTemp = 0;
	while(line[i] != '/' && line[i] != ' ' && i<lenLine){		// segunda parte ptxt4 index (opcional)
		temp[lenTemp++] = line[i];
		i++;
	}
	
	if(lenTemp){												// por ser opcional
		temp[lenTemp] = '\0';
		#if DEBUG >1
		printf("PTXT4 %s\n", temp);
		#endif
		*(arrayTxt +3) = atoi(temp);
	}
	else
		*(arrayTxt +3) = 0;

	/*while(line[i] != ' ')										// ignora vn index e vai para proximo "ponto" da face
		i++;
	*/
	i++;
	lenTemp = 0;
	
	#if DEBUG == 3
	printf("LINE[i-1]: %c (%d)\n", line[i-1], line[i-1]);
	printf("LINE[i]: %c (%d)\n", line[i], line[i-1]);
	#endif
		
	if ( line[i-1] !=' ' && i<lenLine)
	{
		while(i<lenLine-1){										// terceira parte vn index
			temp[lenTemp++] = line[i];
			i++;
		}
	}
	else
		i--;
	
	if(lenTemp){												// por ser opcional
		temp[lenTemp] = '\0';
		#if DEBUG >1
		printf("NRML4: %s\n", temp);
		#endif
		*(arrayNrml +3) = atoi(temp);
	}
	else
		*(arrayNrml +3) = 0;
	
	#if DEBUG >= 1
	printf("ARRAY PTS: ");
	for (int j=0; j<4; j++){
		printf(" %d ", arrayPts[j]);
	}
	printf("\nARRAY TXT: ");
	for (int j=0; j<4; j++){
		printf(" %d ", arrayTxt[j]);
	}
	printf("\nARRAY NRML: ");
	for (int j=0; j<4; j++){
		printf(" %d ", arrayNrml[j]);
	}	
	scanf("%c");
	#endif
	
	return numAAdicionar;
	
}
