#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <GL/glut.h>
#include "particles.h"
#include "loadTextures.h"
#include "../SOIL/SOIL.h"

#define frand()			((float)rand()/RAND_MAX)
#define MAX_PARTICULAS  50

int timeParticle = 0;

Particle  particula[MAX_PARTICULAS];        // particulas em "cima" do liquido
Particle  flow[12*MAX_PARTICULAS];             // particulas em que saem da lata

void drawParticle(float x,float y, float z , float s);

void initSingleParticleGlass(int i, float px, float py, float pz, float radius){
    GLfloat phi,v;
    GLfloat ps = 0.05;
    float r = frand();
    
    int dir1 = (r < 0.5 ) ? -1 : 1;
    r = frand();
    int dir2 = (r < 0.5 ) ? -1 : 1;

    v     = 0.05*frand()+0.02;
    phi   = frand()*M_PI;		// [0.. pi]
    
    particula[i].size = ps ;		// tamanh de cada particula
    particula[i].x	  = px + dir1*frand()*(radius-2*ps);    
    particula[i].y	  = py ;	                    
    particula[i].z	  = pz + dir2*frand()*(radius-2*ps);	


    particula[i].vx = 0;//dir1 * v * cos(theta) * sin(phi);	// esferico
    particula[i].vy = v/10.0f * cos(phi);
    particula[i].vz = 0;//;dir2 * v * sin(theta) * sin(phi);
    particula[i].ax = 0;//0.001f;
    particula[i].ay = -0.001f;
    particula[i].az = 0;//0.0015f;

    particula[i].r = 0.5f;
    particula[i].g = 0.35f;	
    particula[i].b = 0.25f;	
    particula[i].life = 0.8f;		                
    particula[i].fade = 0.01f;	// Em 100=1/0.01 iteracoes desaparece    
}

void initSingleParticleFlow(int i, float px, float py, float pz){
    GLfloat ps = 0.025;
      
    flow[i].vx = 0.0001*frand()+0.009;//0.004f*frand() ;//* cos(phi) ;//* sin(phi);	// esferico
    flow[i].vy = 0;//- ( v/10.0f * cos(phi) );
    flow[i].vz = 0.0f; //dir2 * v * sin(theta) * sin(phi);
    flow[i].ax = 0;// 0.001f*frand() ;//0.01f;
    flow[i].ay = -0.001f*frand()-0.001f;
    flow[i].az = 0.0f;//0.0015f;

    flow[i].size  = ps ;		// tamanh de cada particula
    flow[i].x	  = px ;    
    flow[i].y	  = py ;	                    
    flow[i].z	  = pz ;//+ dir2*frand()*(radius-2*ps);	
    
    if ( frand() < 0.5f  ){
        flow[i].x    += 2*flow[i].vx;
        flow[i].y    += 2*flow[i].vy;
        flow[i].z    += 2*flow[i].vz;
        flow[i].vx   += 2*flow[i].ax;
        flow[i].vy   += 2*flow[i].ay;
        flow[i].vz   += 2*flow[i].az;   

        if ( frand() < 0.5f  ){ 
            flow[i].x    += 2*flow[i].vx;
            flow[i].y    += 2*flow[i].vy;
            flow[i].z    += 2*flow[i].vz;
            flow[i].vx   += 2*flow[i].ax;
            flow[i].vy   += 2*flow[i].ay;
            flow[i].vz   += 2*flow[i].az;
        }
    }
    
    flow[i].r = 0.5f;       
    flow[i].g = 0.35f;	
    flow[i].b = 0.25f;	
    flow[i].life = 1.0f;		                
    flow[i].fade = 0.00f;	// Em 100=1/0.01 iteracoes desaparece    
}

void initParticles()
{
    int i;
    
    for(i=0; i<MAX_PARTICULAS; i++)  {

        //---------------------------------  
        initSingleParticleGlass(i, 21.5f, 0.01f, 15.0f, 0.0f);
    }

     for(i=0; i< 12*MAX_PARTICULAS; i++)  {
        initSingleParticleFlow(i, 21.0f, 4.0f, 15.0f);
     }

}


void drawParticleSystemGlass(GLuint texture, float x, float h, float z, float radius){       // h <=> y
    int i;
    
    glDisable(GL_CULL_FACE);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);      // texture[10] == textura da particula
    glEnable(GL_BLEND);	        		
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);	
    
    

    if ( timeParticle < 2000 )
    {
        for (i=0; i< MAX_PARTICULAS; i++)
        {
            //glColor4f(0.1f, 0.03f, 0.02f, particula[i].life);
            glColor4f(0.5f,0.35f,0.25f, particula[i].life);
            
            drawParticle(particula[i].x, particula[i].y, particula[i].z, particula[i].size);
        
            particula[i].x    += particula[i].vx;
            particula[i].y    += particula[i].vy;
            particula[i].z    += particula[i].vz;
            particula[i].vx   += particula[i].ax;
            particula[i].vy   += particula[i].ay;
            particula[i].vz   += particula[i].az;
            particula[i].life -= particula[i].fade;

            if ( particula[i].life < 0 || particula[i].y < h ){
                initSingleParticleGlass(i, x, h, z, radius);
            }
            
        }

        timeParticle ++;
    }
    

    glDisable(GL_TEXTURE_2D);
    glDisable(GL_BLEND);
    glEnable(GL_CULL_FACE);	 
}

void drawParticleSystemFlow(GLuint texture, float x, float y, float z, float h){       // h <=> y do liquido
    int i;
    
    glDisable(GL_CULL_FACE);
    //glEnable(GL_TEXTURE_2D);
    //glBindTexture(GL_TEXTURE_2D, texture);      // texture[10] == textura da particula
    glEnable(GL_BLEND);	        		
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);	
    
    for (i=0; i<12*MAX_PARTICULAS; i++)
    {
        glColor4f(0.1f, 0.03f, 0.02f, 0.8f);
        //glColor4f(0.5f,0.35f,0.25f, flow[i].life);
        drawParticle(flow[i].x, flow[i].y, flow[i].z, flow[i].size);
        
        flow[i].x    += flow[i].vx;
        flow[i].y    += flow[i].vy;
        flow[i].z    += flow[i].vz;
        flow[i].vx   += flow[i].ax;
        flow[i].vy   += flow[i].ay;
        flow[i].vz   += flow[i].az;
        flow[i].life -= flow[i].fade;

        if ( /*flow[i].life < 0 || */flow[i].y < h ){
            initSingleParticleFlow(i, 21.0f, 4.0f, 15.0f);
        }
        
    }


    //glDisable(GL_TEXTURE_2D);
    glDisable(GL_BLEND);	 
    glEnable(GL_CULL_FACE);
}


void drawParticle(float x, float y, float z, float size){
    
    glPushMatrix();
        glTranslatef(x,y,z);
        glutSolidSphere( size,  8, 8);
    glPopMatrix();
    
}