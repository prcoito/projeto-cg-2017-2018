#include "camera.h"
#include "math.h"
#include "windows.h"
#include <stdio.h>
#include <stdlib.h>

#define	GODMODE 0
//  http://www.codecolony.de/docs/camera2.htm

int sitDown = 0;

#define SQR(x) (x*x)

#define NULL_VECTOR F3dVector(0.0f,0.0f,0.0f)

SF3dVector F3dVector ( GLfloat x, GLfloat y, GLfloat z )
{
	SF3dVector tmp;
	tmp.x = x;
	tmp.y = y;
	tmp.z = z;
	return tmp;
}

GLfloat GetF3dVectorLength( SF3dVector * v)
{
	return (GLfloat)(sqrt(SQR(v->x)+SQR(v->y)+SQR(v->z)));
}

SF3dVector Normalize3dVector( SF3dVector v)
{
	SF3dVector res;
	float l = GetF3dVectorLength(&v);
	if (l == 0.0f) return NULL_VECTOR;
	res.x = v.x / l;
	res.y = v.y / l;
	res.z = v.z / l;
	return res;
}

SF3dVector operator+ (SF3dVector v, SF3dVector u)
{
	SF3dVector res;
	res.x = v.x+u.x;
	res.y = v.y+u.y;
	res.z = v.z+u.z;
	return res;
}
SF3dVector operator- (SF3dVector v, SF3dVector u)
{
	SF3dVector res;
	res.x = v.x-u.x;
	res.y = v.y-u.y;
	res.z = v.z-u.z;
	return res;
}


SF3dVector operator* (SF3dVector v, float r)
{
	SF3dVector res;
	res.x = v.x*r;
	res.y = v.y*r;
	res.z = v.z*r;
	return res;
}

SF3dVector CrossProduct (SF3dVector * u, SF3dVector * v)
{
	SF3dVector resVector;
	resVector.x = u->y*v->z - u->z*v->y;
	resVector.y = u->z*v->x - u->x*v->z;
	resVector.z = u->x*v->y - u->y*v->x;

	return resVector;
}
float operator* (SF3dVector v, SF3dVector u)	//dot product
{
	return v.x*u.x+v.y*u.y+v.z*u.z;
}




/***************************************************************************************/

CCamera::CCamera()
{
	//Init with standard OGL values:
	Position = F3dVector (0.0, 8.0, -10.0);	 	// Posicao incial da camera...
	ViewDir = F3dVector( 0.0, 0.0, 1.0);		// a olhar na direcao do eixo dos zz
	RightVector = F3dVector (1.0, 0.0, 0.0);	
	UpVector = F3dVector (0.0, 1.0, 0.0);

	//Only to be sure:
	RotatedX = RotatedY = RotatedZ = 0.0;
}

void CCamera::Move (SF3dVector Direction)
{
	Position = Position + Direction;
}

void CCamera::RotateX (GLfloat Angle)
{
	RotatedX += Angle;
	
	//Rotate viewdir around the right vector:
	ViewDir = Normalize3dVector(ViewDir*cos(Angle*PIdiv180)
								+ UpVector*sin(Angle*PIdiv180));

	//now compute the new UpVector (by cross product)
	UpVector = CrossProduct(&ViewDir, &RightVector)*-1;

	
}

void CCamera::RotateY (GLfloat Angle)
{
	RotatedY += Angle;
	
	//Rotate viewdir around the up vector:
	ViewDir = Normalize3dVector(ViewDir*cos(Angle*PIdiv180)
								- RightVector*sin(Angle*PIdiv180));

	//now compute the new RightVector (by cross product)
	RightVector = CrossProduct(&ViewDir, &UpVector);
}

void CCamera::RotateZ (GLfloat Angle)
{
	RotatedZ += Angle;
	
	//Rotate viewdir around the right vector:
	RightVector = Normalize3dVector(RightVector*cos(Angle*PIdiv180)
								+ UpVector*sin(Angle*PIdiv180));

	//now compute the new UpVector (by cross product)
	UpVector = CrossProduct(&ViewDir, &RightVector)*-1;
}

void CCamera::Render( void )
{
	#if GODMODE == 0
	/* Apenas deixa andar dentro do quarto*/
	if (Position.z > 14)
		Position.z = 14;
	if (Position.z < -14)
		Position.z = -14;
	if (Position.x > 14)
		Position.x = 14;
	if (Position.x < -14)
		Position.x = -14;

	// colisao com lado (cabeceira) da mesa
	if ( Position.z >= -2.5 && Position.z <=-1 ){
		if (Position.x >= -5 && Position.x<=5){
			Position.z = -2.5;
		}
	}
	else if ( Position.z >=2 && Position.z <=4 ){
		if (Position.x >= -5 && Position.x<=5){
			Position.z = 4;
		}
	}
	// colisao com comprimento mesa
	else if (Position.x >= -5 && Position.x<=0){
		if( Position.z >=-2.5 && Position.z <=4 ){
			Position.x = -5;
		}
	}
	else if (Position.x >= 0 && Position.x<=5){
		if( Position.z >=-2.5 && Position.z <=4 ){
			Position.x = 5;
		}
	}
	// colisao com o sofa
	if ( Position.x >= -5.0f && Position.x <= 8.0f ){
		if ( Position.z <= -15.0f + 4.0f)
			Position.z = -15.0f + 4.0f;
	}


	#endif
	if ( sitDown == 0)
		Position.y = 8;	// altura da camera 8 se de pé
	else
		Position.y = 5;	// altura da camera 4 se sentado


	UpVector.x = UpVector.z = 0; UpVector.y = 1; // e up = eixo yy
	//The point at which the camera looks:
	SF3dVector ViewPoint = Position+ViewDir;
	
	if (ViewPoint.y > 15)     	// apenas pode olhar para o teto de "frente" ( nao há cenas tipo Exorcista )
		ViewPoint.y = 15;
	if (ViewPoint.y < -15)		// apenas pode olhar para o chao de "frente" ( nao há cenas tipo Exorcista )
		ViewPoint.y = -15;
		
	//as we know the up vector, we can easily use gluLookAt:
	gluLookAt(	Position.x,Position.y,Position.z,
				ViewPoint.x,ViewPoint.y,ViewPoint.z,
				UpVector.x,UpVector.y,UpVector.z);

}

void CCamera::MoveForward( GLfloat Distance )
{
	Position = Position + (ViewDir*-Distance);
}

void CCamera::StrafeRight ( GLfloat Distance )
{
	Position = Position + (RightVector*Distance);
}

void CCamera::MoveUpward( GLfloat Distance )
{
	Position = Position + (UpVector*Distance);
}

void CCamera::print(void){
	printf("Position.x %f, Position.y %f,Position.z %f\n",  Position.x , Position.y , Position.z);
	printf("UpVector.x %f, UpVector.y %f,UpVector.z %f\n",  UpVector.x , UpVector.y , UpVector.z  );
	
}

float CCamera::getPositionX(void){
	return Position.x;
}

float CCamera::getPositionZ(void){
	return Position.z;
}
