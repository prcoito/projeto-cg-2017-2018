#include <GL/glut.h>

//---------------------------------------- Particle attributes
typedef struct {
	float   size;		// tamanho
	float	life;		// vida
	float	fade;		// fade
	float	r, g, b;    // color
	GLfloat x, y, z;    // posicao
	GLfloat vx, vy, vz; // velocidade 
    GLfloat ax, ay, az; // aceleracao
} Particle;


void drawParticleSystemGlass(GLuint texture, float px, float py, float pz, float radius);
void drawParticleSystemFlow(GLuint texture, float px, float py, float pz, float h);

void initParticles();