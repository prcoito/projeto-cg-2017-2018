#include <GL/glut.h>

#ifndef Structs
#define Structs
	typedef struct Face{
		unsigned short vertex[4]; 		// guarda indices dos vertices [ no maximo só aceita faces quadradas (para simplificar)]
		unsigned short txtPts[4]; 		// guarda indices das coordenadas dos vertices respetivos [ no maximo só aceita faces quadradas (para simplificar)]
		unsigned short normalVec[4];	// guarda indices das normais dos vertices respetivos [ no maximo só aceita faces quadradas (para simplificar)]
		unsigned short numVertices; 	// == numTextPts == numNormalVec 
	} Face;

	typedef struct MiniObjeto{
		Face *faces;	// lista de todas as faces do objeto
		unsigned short numFaces;
	} MiniObjeto;

	typedef struct Objeto{
		MiniObjeto *mObjecto;				// lista de mini-objetos de um objeto 3d (>=1)
		unsigned short numObjetos;			// tamanho do array de cima
		double *vertex;						// lista de todos os vertices do objeto 3d
		unsigned short numVertex;			// num total de vertices
		double *txtPts;						// lista de coordenadas para o mapeamento de texturas ( s,t [r,q ignorados pois raramente se usam e nos objetos que temos nao aparece])
		unsigned short numTxtPts;			// num de coordenadas do array de cima
		double *normals;					// lista de normais de vertices
		unsigned short numNormals;			// num de normais do array de cima
		char filename[40];					// localizacao do ficheiro [para facilitar a identificacao do objeto 3d]
	} Objeto;
#endif


void loadObjs(Objeto *lista);
Objeto loadObj(char *filename);
void freeObjs(Objeto *lista);
void freeObjs(Objeto *lista, int total);

//void printObj(Objeto modelo);
