#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "loaderModels.h"
#include "loadTextures.h"
#include "../SOIL/SOIL.h"

#define TOTAL_TEXTURES_EXTRA 		4
#define HOUSE_PLANT			        0

const char ListTextures_extra[][45] = {
	"Textures\\Extras\\HousePlant.jpg",
	"Textures\\Extras\\Tree.jpg",
	"Textures\\Extras\\Chair.jpg",
	"Textures\\Extras\\Sofa.jpg"
};

#define TOTAL_MODELS_EXTRA  		4
#define HOUSE_PLANT	        		0

char loc_models_extra[][40] = {				// localizacao dos modelos a serem carregados
	"3D_Models\\Extras\\HousePlant.obj",
	"3D_Models\\Extras\\Tree.obj",
	"3D_Models\\Extras\\Chair.obj",
	"3D_Models\\Extras\\Sofa.obj"
};


void loadObjsExtra(Objeto *lista){
	int numObjetosLoaded = 0;
	char filename[40];
	
	for ( int i = 0; i<TOTAL_MODELS_EXTRA; i++){
		strcpy(filename, loc_models_extra[i]);
		lista[i] = loadObj(filename);
		if ( lista[i].numVertex )						// se nao houve erro, ou seja numVertex > 0
			strcpy(lista[i].filename, filename);
		else
		{
			freeObjs(lista, numObjetosLoaded);
			exit(0);
		}
		numObjetosLoaded++;
	}
}

void loadTexturesExtra(GLuint *texture)
{   
	char filename[40];
	
    for (int i=0; i<TOTAL_TEXTURES_EXTRA; i++)
	{
		strcpy(filename, ListTextures_extra[i]);
		texture[i] = SOIL_load_OGL_texture(filename, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);
		if (texture[i] == 0)
			printf("ERROR READING TEXTURE %s\n", filename);
		
		glBindTexture(GL_TEXTURE_2D, texture[i]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
	}
}