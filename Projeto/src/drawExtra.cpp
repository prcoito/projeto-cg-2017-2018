#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "loaderModels.h"
#include "draw.h"
#include "loadTextures.h"

#define TOTAL_TEXTURES_EXTRA 		4
#define HOUSE_PLANT			        0

#define TOTAL_MODELS_EXTRA  		4
#define HOUSE_PLANT	        		0

#define DEBUGEX 0

int nRepeat[][2] = {
    // numero de repeticoes do modelos i-1, e total de modelos ate i+1
    {0, 0},
    {2, 0},
    {2, 2},
    {4, 4},
    {1, 8}
};

float repeatE[] = {	
// repeticao da textura em:
//   x     y	
	1.0f, 1.0f,			// plant1
	1.0f, 1.0f,			// plant2
    1.0f, 1.0f,			// tree2
    1.0f, 1.0f,			// tree2
    1.0f, 1.0f,			// chair1
    1.0f, 1.0f,			// chair2
    1.0f, 1.0f,			// chair3
    1.0f, 1.0f,			// chair4
    1.0f, 1.0f			// sofa
};

float rotatesE[] = {
//  angle	 x  	y  	 z  
	0.0f, 	0.0f, 1.0f, 0.0f,	        // plant1
	0.0f, 	0.0f, 0.0f, 1.0f,	        // plant2
    -2.5f, 	1.0f, 0.0f, 0.0f,			// tree2
    -2.5f, 	1.0f, 0.0f, 0.0f,			// tree2
    30.0f, 	0.0f, 1.0f, 0.0f,	        // chair1
    210.0f, 	0.0f, 1.0f, 0.0f,	    // chair2
    30.0f, 	0.0f, 1.0f, 0.0f,	        // chair3
    210.0f, 	0.0f, 1.0f, 0.0f,	    // chair4
    0.0f, 	0.0f, 1.0f, 0.0f,	        // sofa

};

float translatesE[] = {
//    x  	 y  	  z    
	2.0f,	0.1f, 	1.5f,		// plant1
    28.0f,	0.1f, 	1.5f,		// plant2
    5.0f, 0.1f, 36.0f,		    // tree1
    25.0f, 0.1f, 36.0f,		    // tree2
    10.00f, 0.04f, 12.50f,		// chair1
    17.40f, 0.04f, 17.50f,		// chair2
    14.00f, 0.04f, 12.50f,		// chair3
    21.50f, 0.04f, 17.50f,		// chair4
    11.00f, 0.04f, 2.50f,		// sofa
};

float scalesE[] = {
//     sx       sy       sz
	0.004f, 0.004f, 0.004f,		// plant1
    0.004f, 0.004f, 0.004f,		// plant2
    2.0f, 2.0f, 2.0f,		    // tree1
    2.0f, 2.0f, 2.0f,		    // tree2
    6.0f, 8.0f, 5.0f,		    // chair1
    6.0f, 8.0f, 5.0f,		    // chair2
    6.0f, 8.0f, 5.0f,		    // chair3
    6.0f, 8.0f, 5.0f,		    // chair4
    2.0f, 2.0f, 1.5f            // sofa
};


void drawModels(Objeto *mExtra, GLuint *textExtra);
void drawExtras(Objeto *mExtra, GLuint *textExtra){
    glPushMatrix();
        glTranslatef(-15, 0, -15);
        drawModels(mExtra, textExtra);
    glPopMatrix();
}

void drawModels(Objeto *modelo, GLuint *texture){
    //glDisable(GL_CULL_FACE);

    for(int i = 0; i< TOTAL_TEXTURES_EXTRA; i++){
        #if DEBUGEX
        printf("---------------------------------\n");
        #endif
        for(int j = 0; j < nRepeat[i+1][0]; j++){
            #if DEBUGEX
            printf("modelo[i].filename %s\n", modelo[i].filename );
            printf("translatesE[3*(nRepeat[i]+j)   ] %.2f\n", translatesE[3*(nRepeat[i+1][1]+j)   ] );
            printf("translatesE[3*(nRepeat[i]+j) +1] %.2f\n", translatesE[3*(nRepeat[i+1][1]+j) +1] );
            printf("translatesE[3*(nRepeat[i]+j) +2] %.2f\n", translatesE[3*(nRepeat[i+1][1]+j) +2] );
            printf("scalesE[3*(nRepeat[i]+j)   ] %.2f\n", scalesE[3*(nRepeat[i+1][1]+j)   ] );
            printf("scalesE[3*(nRepeat[i]+j) +1] %.2f\n", scalesE[3*(nRepeat[i+1][1]+j) +1] );
            printf("scalesE[3*(nRepeat[i]+j) +2] %.2f\n", scalesE[3*(nRepeat[i+1][1]+j) +2]  );
            printf("rotatesE[4*(nRepeat[i]+j)   ] %.2f\n", rotatesE[4*(nRepeat[i+1][1]+j)   ] );
            printf("rotatesE[4*(nRepeat[i]+j) +1] %.2f\n", rotatesE[4*(nRepeat[i+1][1]+j) +1]  );
            printf("rotatesE[4*(nRepeat[i]+j) +2] %.2f\n", rotatesE[4*(nRepeat[i+1][1]+j) +2] );
            printf("rotatesE[4*(nRepeat[i]+j) +3] %.2f\n", rotatesE[4*(nRepeat[i+1][1]+j) +3] );
            printf("repeatE[2*(nRepeat[i]+j)   ] %.2f\n", repeatE[2*(nRepeat[i+1][1]+j)   ] );
            printf("repeatE[2*(nRepeat[i]+j) +1] %.02f\n", repeatE[2*(nRepeat[i+1][1]+j) +1] );
            #endif

            draw3DModel(modelo[i],
                        texture[i],
                        translatesE[3*(nRepeat[i+1][1]+j)   ],
                        translatesE[3*(nRepeat[i+1][1]+j) +1],
                        translatesE[3*(nRepeat[i+1][1]+j) +2],
                        scalesE[3*(nRepeat[i+1][1]+j)   ],
                        scalesE[3*(nRepeat[i+1][1]+j) +1],
                        scalesE[3*(nRepeat[i+1][1]+j) +2],
                        rotatesE[4*(nRepeat[i+1][1]+j)   ],
                        rotatesE[4*(nRepeat[i+1][1]+j) +1],
                        rotatesE[4*(nRepeat[i+1][1]+j) +2],
                        rotatesE[4*(nRepeat[i+1][1]+j) +3],
                        repeatE[2*(nRepeat[i+1][1]+j)   ],
                        repeatE[2*(nRepeat[i+1][1]+j) +1]);

        }
	}
}