#include <GL/glut.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include "draw.h" // para saber x_bola

#define MAP 1
#define HUD 2

#define BLACK 0.0f, 0.0f, 0.0f, 1.0f
extern int sitDown;

void drawString(char *string, int len, int size);

void drawBorders(int tipo ){
    float previousColor[4];
    char texto[50];

	glGetFloatv(GL_CURRENT_COLOR,  previousColor);
    glColor4f(BLACK);

    if (tipo == MAP)
    {
    	glBegin(GL_QUADS);
            glVertex3i(-30, -1, -30);
            glVertex3i(-30, -1,  30);
            glVertex3i( 30, -1,  30);
            glVertex3i( 30, -1, -30);
        glEnd();
    }
    
    glColor4fv(previousColor);
}


void drawHUD(float x_camera, float z_camera){
    char texto[30];
    
                                                                   // z_bola = 0
    if ( (fabs( x_camera+15.0f - x_bola )<= 3) && (fabs( z_camera - 0  )<= 3) && !keyPressed)
    {
        sprintf(texto, "Prima 'E' para interagir");
        drawString(texto,strlen(texto), 18);
    }

    if ( ( x_camera + 15.0f >= 10.0f ) && ( x_camera +15.0f <= 21.0f )  &&  
	     ( z_camera + 15.0f <= 4 )                                            )
    {
        if ( sitDown )
            sprintf(texto, "Prima 'E' para levantar.");
        else
            sprintf(texto, "Prima 'E' para sentar.");
        drawString(texto,strlen(texto), 18);
    }

    if ( (fabs( x_camera+15.0f - 19.5f )<= 2.0f) && (fabs( z_camera - 0.0f  )<= 2.0f) )
    {
        sprintf(texto, "Prima 'R' para fazer Reset");
        drawString(texto,strlen(texto), 18);
    }

    sprintf(texto, "Andar: A|Esq, D|Dir, S|Tras, W|Frente");
    drawString(texto,strlen(texto), 80);
    
    sprintf(texto, "Reset: R");
    drawString(texto,strlen(texto), 60);

    sprintf(texto, "Interagir: E");
    drawString(texto,strlen(texto), 40);
    
    sprintf(texto, "Zoom in/out: I/O");
    drawString(texto,strlen(texto), 20);
    
    sprintf(texto, "Blend plus/minus: K/L");
    drawString(texto,strlen(texto), 0);
    

}

void drawString(char *string, int len, int size)
{
    float previousColor[4];
    glGetFloatv(GL_CURRENT_COLOR,  previousColor);
    glColor4f(BLACK);

    int w = glutGet( GLUT_WINDOW_WIDTH );
    int h = glutGet( GLUT_WINDOW_HEIGHT );

    if ( size == 18)
        glViewport (w/2-8*len/2, w/2+8*len/2, 3*h/4, 3*h/4+5);
    else
        glViewport (3*w/4-20, 3*h/4, 5*h/6, 5*h/6+size);
    glMatrixMode(GL_PROJECTION);
    
    glPushMatrix();             
        glLoadIdentity();   
        glOrtho( 0, w, 0, h, -1, 1 );
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glLoadIdentity();
        if ( size == 18 ){
            glRasterPos2i(0, 10);
            while(*string)
                glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *string++); 
        }
        else{
            glRasterPos2i(0, size); 
            while(*string)
                glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, *string++); 
        }

        glPopMatrix();
    
    glPopMatrix();  

    glColor4fv(previousColor);
}