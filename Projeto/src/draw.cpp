#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <GL/glut.h>
#include "loaderModels.h"
#include "motion.h"
#include "draw.h"		// para var keyPressed
#include "particles.h"

#define NUM_QUAD				5

#define TOTAL_TEXTURES 			10
#define CAN_TEXTUTE				0
#define LAMP_TEXTURE			1
#define WINDOW_TEXTURE			2
#define TABLE_TEXTURE			3
#define INTERIOR_WALL_TEXTURE 	4
#define FLOOR_TEXTURE 			5
#define ROOF_TEXTURE			6
#define SKYBOX_TEXTURE			7
#define TERRAIN_TEXTURE			8
#define CRACK_TEXTURE			9

#define TOTAL_MODELS 5

#define COR_PAREDE	1.0f, 0.94f, 0.64f,	1.0f	// cor da parede
#define COKECOLOR	0.1f, 0.03f, 0.02f,0.8f 	// cor coca-cola 
#define GREEN 		0.0f, 1.0f, 0.0, 1.0f		// cor bola

#define POS_ANGULO_LATA 		0		// posicao do array rotates onde está guardado o valor do angulo da lata
#define POS_X_LATA 				0		// posicao do array translates onde está guardado o valor do x da lata ...
#define POS_Y_LATA 				1		// ... e onde está o valor de y da lata

// movimento bola
// x_inicial e y_inicial usados para passar a equacao do movimento da bola.
float time = 0.0f;
float x_bola = 10.0f,x_inicial = 10.0f;
float y_bola = 4.2f ,y_inicial = 5.5f;		 // y_bola = 4.2f para nao estar suspensa no ar
float velocity = 40.0f;
float bounce = -1.0;		// bounce serve fazer a bola subir ou descer, dependendo da parte da animacao
float MAX_ALT = 7.0f;		// Apos o primeiro "estado" da animacao, a bola só pode 
							// subir até MAX_ALT ( alterado durante a execucao para conferir mais realismo) 
int Colision = 0;			// detecao da colisao da bola com a lata

boolean CAN_NOT_EMPTY = true, drawCone = false;
float radiusCone = 0.3f;
float hCone = 0.1f;
void drawConeBase(float raio, float y);

int cracks[12];

extern int timeParticle;



float text_mapTextModel[] = {	// mapeanto textura vertice objeto
//	 s	   t
	0.0f, 0.0f,
	1.0f, 0.0f,
	1.0f, 1.0f,
	0.0f, 1.0f
};

float repeat[] = {	
// repeticao da textura em:
//   x     y	
	1.0f, 1.0f,			// table
	1.0f, 1.0f,			// can
	1.0f, 2.0f,			// Lamp
	1.0f, 1.0f			// window
};

float rotates[] = {
//  angle	 x  	y  	 z  
	0.0f, 	0.0f, 0.0f, 1.0f,	// can
	0.0f, 	0.0f, 1.0f, 0.0f,	// Lamp
	90.0f, 	0.0f, 1.0f, 0.0f,	// window
	0.0f, 	0.0f, 1.0f, 0.0f,	// Glass
	0.0f, 	0.0f, 1.0f, 0.0f	// table
};

float translates[] = {
//    x  	 y  	  z    
	19.5f, 	4.3f, 	15.0f,		// can
	15.0f, 	12.5f, 	15.0f,		// Lamp
	15.0f, 	9.10f, 	29.98f,		// window
	21.5f, 	0.01f, 	15.0f,		// glass
	15.0f,	0.0f, 	15.0f		// table
};

float scales[] = {
//   sx     sy     sz
	0.35f,	0.35f, 	0.35f,		// can
	0.002f, 0.002f, 0.002f,		// Lamp
	0.1f, 	0.11f,	0.001f,		// window
	7.0f,	7.0f,	7.0f,		// glass
	0.05f, 	0.05f, 	0.05f		// table
};

//float corCopo[] = {1.0f, 1.0f, 1.0f, 0.3f}; // 77.6, 88.6, 89
float corCopo[] = {0.776f, 0.886f, 0.89f, 0.3f};

void drawSphere(GLuint texture);
void drawTerreno(GLuint *texture, int xC);
void drawRoom(GLuint *texture, int xC);
void drawSkyBox(GLuint *texture, int xC);
												// translate x, y,z
void draw3DModel(Objeto modelo, GLuint texture, float x, float y, float z, 
												// scale sx, sy, sz
												float sx, float sy, float sz, 
												// rotate angle, rx, ry, rz
												float angle, float rx, float ry, float rz,
												// repete texture em x e y
												float repeat_x, float repeat_y);
void drawCracksInWalls(GLuint *texture, int xC);
void drawWindowWindow();


void drawScene(GLuint *texture){
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE);
	drawSkyBox(texture, 15);
	drawTerreno(texture, 15);
	glTranslatef(-15, 0, -15);	// para deslocar tudo ( sala, objetos 3d, etc)
	drawRoom(texture, 15);
	drawCracksInWalls(texture, 15);

	drawSphere(texture[10]);
	
}

void draw3DModels(Objeto *modelo, GLuint *texture)
{
	for(int i = 0; i< TOTAL_MODELS; i++){
		draw3DModel(modelo[i],
					 (i<3) ? texture[i] : (i==4) ? texture[TABLE_TEXTURE] : 0,		// se copo (i==3) nao "envia" textura se i==4 envia textura[3] == textura mesa
					translates[3*i  ],
					translates[3*i+1],
					translates[3*i+2],
					scales[3*i  ],
					scales[3*i+1],
					scales[3*i+2],
					rotates[4*i  ],
					rotates[4*i+1],
					rotates[4*i+2],
					rotates[4*i+3],
					repeat[2*i  ],
					repeat[2*i+1]);
	}

	drawWindowWindow();
}

void drawWindowWindow(){
	GLfloat previousColor[4];
	glEnable (GL_BLEND); 
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 	// para dar a transparencia ao vidro da janela (Funcao de mistura)
	glGetFloatv(GL_CURRENT_COLOR,  previousColor);			// guarda cor anterior
	glColor4fv(corCopo);

	glPushMatrix();
		// vidro da janela maior
		glBegin(GL_QUADS);
			glVertex3f( 13.0f,  5.3f, 30.0f);  
			glVertex3f( 13.0f, 10.6f, 30.0f); 
			glVertex3f( 17.0f, 10.6f, 30.0f);
			glVertex3f( 17.0f,  5.3f, 30.0f); 
		glEnd();
		// vidro da janela menor
		glBegin(GL_QUADS);
			glVertex3f( 13.0f, 10.5f, 30.0f);  
			glVertex3f( 13.0f, 13.5f, 30.0f); 
			glVertex3f( 17.0f, 13.5f, 30.0f);
			glVertex3f( 17.0f, 10.5f, 30.0f); 
		glEnd();
	glPopMatrix();

	glDisable (GL_BLEND); 
	glColor4fv(previousColor);
}

void draw3DModel(Objeto modelo, GLuint texture, float x, float y, float z, 
												float sx, float sy, float sz, 
												float angle, float rx, float ry, float rz,
												float repeat_x, float repeat_y){
    GLfloat previousColor[4];
	int v_index, txt_index, nrml_index;
	
	if ( modelo.numNormals == 0 )
		glDisable(GL_CULL_FACE); 	// visto que ainda nao está implementado a parte da leitura e armazenamento das normais
								// de cada um dos vertices dos objetos 3d. tem que se desativar o CULL_FACE para desenhar
								// todos os vertices/faces do objeto
	if (texture!=0)
	{
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, texture);
		
	}
	else // copo
	{
		glEnable (GL_BLEND); 
		glDisable(GL_LIGHTING);
		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 	// para dar a transparencia ao copo (Funcao de mistura)
		glGetFloatv(GL_CURRENT_COLOR,  previousColor);			// guarda cor anterior
		glColor4fv(corCopo);
	}
	if ( strcmp(modelo.filename, "3D_Models\\Lamp.obj") == 0 || strcmp(modelo.filename, "3D_Models\\Table.obj") == 0 )
	{
		glEnable (GL_BLEND);
		glDisable(GL_LIGHTING);
		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 	// para dar a transparencia ao copo (Funcao de mistura)
		
	}

	glPushMatrix();
		glTranslatef(x,y,z);
        glScalef(sx,sy,sz);
		glRotatef(angle,rx,ry,rz);
        
		// desenho do objeto.
		// por cada mini-objeto(parte do objeto [perna da mesa por exemplo]) desenha as respetivas faces
		// podem ter 3 ou 4 pontos. 
        for(int i = 0; i< modelo.numObjetos; i++){
            for (int j = 0; j< modelo.mObjecto[i].numFaces; j++){
                glBegin(GL_POLYGON);
                    for( int k = 0; k<modelo.mObjecto[i].faces[j].numVertices; k++)
                    {
                        v_index   = modelo.mObjecto[i].faces[j].vertex[k];
						txt_index = modelo.mObjecto[i].faces[j].txtPts[k];
						nrml_index = modelo.mObjecto[i].faces[j].normalVec[k];
						if ( modelo.numNormals >= 3){
							glNormal3f( modelo.normals[3*nrml_index], modelo.normals[3*nrml_index+1], modelo.normals[3*nrml_index+2] );
						}

						if (txt_index >= 0 && modelo.numTxtPts > 2){	
							//printf("Model = %s\n", modelo.filename);
							glTexCoord2f( modelo.txtPts[2*txt_index], 
									  	  modelo.txtPts[2*txt_index+1]);
						}
						else													// NOT USED
							glTexCoord2f( repeat_x * text_mapTextModel[2 * k], 
									  	  repeat_y * text_mapTextModel[2*k+1]);
						//else
						//	printf("%s modelo.numTxtPts>0 = %d\n", modelo.filename, modelo.numTxtPts>0);
                        glVertex3f(modelo.vertex[3*v_index], modelo.vertex[3*v_index+1], modelo.vertex[3*v_index+2]);   
                    }
                glEnd();
            }
        }
    
    glPopMatrix();
	// repor estados iniciais
    if (texture!=0)
		glDisable(GL_TEXTURE_2D);
	else // copo
	{
		glDisable (GL_BLEND); 
		glColor4fv(previousColor);
		glEnable(GL_LIGHTING);
	}
	if ( strcmp(modelo.filename, "3D_Models\\Lamp.obj") == 0 || strcmp(modelo.filename, "3D_Models\\Table.obj") == 0 )
	{
		glDisable (GL_BLEND); 
		glEnable (GL_LIGHTING);
	}
	
	if ( modelo.numNormals == 0 )
	{
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}
}

void drawTerreno(GLuint *texture, int xC){
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texture[TERRAIN_TEXTURE]);
	glPushMatrix();
		//glTranslatef(0, 0, 0);
		glBegin(GL_QUADS);
			glTexCoord2f( 0, 0 );	glVertex3f( (float)-2*xC, -0.1f, (float)-2*xC ); 
			glTexCoord2f( 0, 5 ); 	glVertex3f( (float)-2*xC, -0.1f, (float) 2*xC ); 
			glTexCoord2f( 5, 5 ); 	glVertex3f( (float) 2*xC, -0.1f, (float) 2*xC ); 
			glTexCoord2f( 5, 0 ); 	glVertex3f( (float) 2*xC, -0.1f, (float)-2*xC ); 
		glEnd();
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
}
void drawSkyBox(GLuint *texture, int xC)
{
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SkyBox
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texture[SKYBOX_TEXTURE]);
	glPushMatrix();
		//glTranslatef(0, 0, 0);
		// SkyBox up
		glBegin(GL_QUADS);
			glTexCoord2f( 1*1.0f/4, 0*1.0f/3 ); glVertex3i( -2*xC, 2*xC, -2*xC ); 
			glTexCoord2f( 2*1.0f/4, 0*1.0f/3 ); glVertex3i(  2*xC, 2*xC, -2*xC ); 
			glTexCoord2f( 2*1.0f/4, 1*1.0f/3 ); glVertex3i(  2*xC, 2*xC,  2*xC ); 
			glTexCoord2f( 1*1.0f/4, 1*1.0f/3 ); glVertex3i( -2*xC, 2*xC,  2*xC ); 
		glEnd();
		// SkyBox down
		glBegin(GL_QUADS);
			glTexCoord2f( 1*1.0f/4, 2*1.0f/3 ); glVertex3i( -2*xC, -2*xC, -2*xC ); 
			glTexCoord2f( 2*1.0f/4, 2*1.0f/3 ); glVertex3i(  2*xC, -2*xC, -2*xC ); 
			glTexCoord2f( 2*1.0f/4, 3*1.0f/3 ); glVertex3i(  2*xC, -2*xC,  2*xC ); 
			glTexCoord2f( 1*1.0f/4, 3*1.0f/3 ); glVertex3i( -2*xC, -2*xC,  2*xC ); 
		glEnd();
		// SkyBox left
		glBegin(GL_QUADS);
			glTexCoord2f( 0*1.0f/4, 1*1.0f/3 ); glVertex3i( -2*xC,  2*xC, -2*xC ); 
			glTexCoord2f( 1*1.0f/4, 1*1.0f/3 ); glVertex3i( -2*xC,  2*xC,  2*xC );  
			glTexCoord2f( 1*1.0f/4, 2*1.0f/3 ); glVertex3i( -2*xC, -2*xC,  2*xC ); 
			glTexCoord2f( 0*1.0f/4, 2*1.0f/3 ); glVertex3i( -2*xC, -2*xC, -2*xC );
		glEnd();
		// SkyBox front
		glBegin(GL_QUADS);
			glTexCoord2f ( 1*1.0f/4, 2*1.0f/3 ); glVertex3f ( -2*xC,  -2*xC, -2*xC);
			glTexCoord2f ( 2*1.0f/4, 2*1.0f/3 ); glVertex3f (  2*xC,  -2*xC, -2*xC);
			glTexCoord2f ( 2*1.0f/4, 1*1.0f/3 ); glVertex3f (  2*xC,   2*xC, -2*xC);
			glTexCoord2f ( 1*1.0f/4, 1*1.0f/3 ); glVertex3f ( -2*xC,   2*xC, -2*xC);
		glEnd();
		// SkyBox right
		glBegin(GL_QUADS);
			glTexCoord2f( 2*1.0f/4, 2*1.0f/3 ); glVertex3i( 2*xC, -2*xC, -2*xC );
			glTexCoord2f( 3*1.0f/4, 2*1.0f/3 ); glVertex3i( 2*xC, -2*xC,  2*xC );
			glTexCoord2f( 3*1.0f/4, 1*1.0f/3 ); glVertex3i( 2*xC,  2*xC,  2*xC );
			glTexCoord2f( 2*1.0f/4, 1*1.0f/3 ); glVertex3i( 2*xC,  2*xC, -2*xC );
		glEnd();
		// SkyBox back
		glBegin(GL_QUADS);
			glTexCoord2f ( 3*1.0f/4, 2*1.0f/3 ); glVertex3f (  2*xC,  -2*xC, 2*xC);
			glTexCoord2f ( 4*1.0f/4, 2*1.0f/3 ); glVertex3f ( -2*xC,  -2*xC, 2*xC);
			glTexCoord2f ( 4*1.0f/4, 1*1.0f/3 ); glVertex3f ( -2*xC,   2*xC, 2*xC);
			glTexCoord2f ( 3*1.0f/4, 1*1.0f/3 ); glVertex3f (  2*xC,   2*xC, 2*xC);
		glEnd();
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
}

void drawRoom(GLuint *texture, int xC)
{
	float previousColor[4];

	glEnable(GL_COLOR_MATERIAL);
	glGetFloatv(GL_CURRENT_COLOR,  previousColor);
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Chao y=0
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texture[FLOOR_TEXTURE]);
	glPushMatrix();
		
		for(int i = 0; i< NUM_QUAD; i++){
			for (int j = 0; j< NUM_QUAD; j++){
				glBegin(GL_QUADS);
					glTexCoord2f( i*(10.0f/NUM_QUAD)			 , j*(10.0f/NUM_QUAD)			  );	glVertex3i( i*(2*xC/NUM_QUAD)  		   , 0, j*(2*xC/NUM_QUAD)  			); 
					glTexCoord2f( i*(10.0f/NUM_QUAD)			 , (10.0f/NUM_QUAD)+j*(10.0f/NUM_QUAD));	glVertex3i( i*(2*xC/NUM_QUAD)   		   , 0, (2*xC/NUM_QUAD) + j*(2*xC/NUM_QUAD) );
					glTexCoord2f( (10.0f/NUM_QUAD)+i*(10.0f/NUM_QUAD), (10.0f/NUM_QUAD)+j*(10.0f/NUM_QUAD));	glVertex3i( (2*xC/NUM_QUAD) +i*(2*xC/NUM_QUAD) , 0, (2*xC/NUM_QUAD) + j*(2*xC/NUM_QUAD) ); 
					glTexCoord2f( (10.0f/NUM_QUAD)+i*(10.0f/NUM_QUAD), j*(10.0f/NUM_QUAD)			  );	glVertex3i( (2*xC/NUM_QUAD) +i*(2*xC/NUM_QUAD) , 0, j*(2*xC/NUM_QUAD)    			);  
				glEnd();
			}
		}

	glPopMatrix();
	//glDisable(GL_TEXTURE_2D);

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Parede y = xC
	//glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texture[ROOF_TEXTURE]);
	glPushMatrix();
		
		for(int i = 0; i< NUM_QUAD; i++){
			for (int j = 0; j< NUM_QUAD; j++){
				glBegin(GL_QUADS);
					glTexCoord2f( (10.0f/NUM_QUAD)*i			   , (5.0f/NUM_QUAD)*j			);	glVertex3i( (2*xC/NUM_QUAD)*i			   , xC, (2*xC/NUM_QUAD)*j   			 ); 
					glTexCoord2f( (10.0f/NUM_QUAD) + (10.0f/NUM_QUAD)*i, (5.0f/NUM_QUAD)*j			);	glVertex3i( (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*i, xC, (2*xC/NUM_QUAD)*j   			 ); 
					glTexCoord2f( (10.0f/NUM_QUAD) + (10.0f/NUM_QUAD)*i, (5.0f/NUM_QUAD)*j + (5.0f/NUM_QUAD));	glVertex3i( (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*i, xC, (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*j ); 
					glTexCoord2f( (10.0f/NUM_QUAD)*i			   , (5.0f/NUM_QUAD)*j + (5.0f/NUM_QUAD));	glVertex3i( (2*xC/NUM_QUAD)*i   		   , xC, (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*j ); 
				glEnd();
			}
		}
	glPopMatrix();
	//glDisable(GL_TEXTURE_2D);
	//*****************************************************

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Parede x=0

	//glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texture[INTERIOR_WALL_TEXTURE]);
	glPushMatrix();
		
		for(int i = 0; i< NUM_QUAD; i++){
			for (int j = 0; j< 1; j++){
				glBegin(GL_QUADS);
					glTexCoord2f( (8.0f/NUM_QUAD)*i, (2.0f/1)*j			  );					glVertex3i( 0, (2*xC/NUM_QUAD)*j 		   	  , (2*xC/NUM_QUAD)*i   		    ); 
					glTexCoord2f( (8.0f/NUM_QUAD)*i, (2.0f/1) + (5.0f/1)*j);					glVertex3i( 0, (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*j, (2*xC/NUM_QUAD)*i   		    ); 
					glTexCoord2f( (8.0f/NUM_QUAD) + (8.0f/NUM_QUAD)*i, (2.0f/1) + (2.0f/1)*j);	glVertex3i( 0, (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*j, (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*i ); 
					glTexCoord2f( (8.0f/NUM_QUAD) + (8.0f/NUM_QUAD)*i, (2.0f/1)*j			);	glVertex3i( 0, (2*xC/NUM_QUAD)*j 		      , (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*i );
				glEnd();
			}
		}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Parede z=0
		
		for(int i = 0; i< NUM_QUAD; i++){
			for (int j = 0; j< 1; j++){
				glBegin(GL_QUADS);
					glTexCoord2f( (8.0f/NUM_QUAD)*i, (2.0f/1)*j );									glVertex3i( (2*xC/NUM_QUAD)*i			   , (2*xC/NUM_QUAD)*j  		    , 0	); 
					glTexCoord2f( (8.0f/NUM_QUAD) + (8.0f/NUM_QUAD)*i, (2.0f/1)*j );				glVertex3i( (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*i, (2*xC/NUM_QUAD)*j   		    , 0 );
					glTexCoord2f( (8.0f/NUM_QUAD) + (8.0f/NUM_QUAD)*i, (2.0f/1) + (2.0f/1)*j );	glVertex3i( (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*i, (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*j, 0 ); 
					glTexCoord2f( (8.0f/NUM_QUAD)*i, (2.0f/1) + (2.0f/1)*j );						glVertex3i( (2*xC/NUM_QUAD)*i            , (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*j, 0	); 
				glEnd();
			}
		}
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Parede x=2*xC
		
		for(int i = 0; i< NUM_QUAD; i++){
			for (int j = 0; j< 1; j++){
				glBegin(GL_QUADS);
					glTexCoord2f( (8.0f/NUM_QUAD)*i, (2.0f/1)*j );									glVertex3i( 2*xC, (2*xC/NUM_QUAD)*j, (2*xC/NUM_QUAD)*i ); 
					glTexCoord2f( (8.0f/NUM_QUAD) + (8.0f/NUM_QUAD)*i, (2.0f/1)*j );				glVertex3i( 2*xC, (2*xC/NUM_QUAD)*j, (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*i );
					glTexCoord2f( (8.0f/NUM_QUAD) + (8.0f/NUM_QUAD)*i, (2.0f/1) + (2.0f/1)*j );	glVertex3i( 2*xC, (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*j, (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*i ); 
					glTexCoord2f( (8.0f/NUM_QUAD)*i, (2.0f/1) + (2.0f/1)*j );						glVertex3i( 2*xC, (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*j, (2*xC/NUM_QUAD)*i ); 
				glEnd();
			}
		}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Parede z=2*xC
	// desenho das varias partes da parede. Comeca pela parte antes da janela
	// desenha a parte por baixo
	// desenha a restante parte da janela
		
		for(int i = 0; i< 2; i++){
			for (int j = 0; j< 1; j++){
				glBegin(GL_QUADS);
					glTexCoord2f( (8.0f/NUM_QUAD)*i, (2.0f/1)*j );								glVertex3i( (2*xC/NUM_QUAD)*i   		       , (2*xC/NUM_QUAD)*j 		 			, 2*xC ); 
					glTexCoord2f( (8.0f/NUM_QUAD)*i, (2.0f/1) + (2.0f/1)*j );					glVertex3i( (2*xC/NUM_QUAD)*i			   	   , (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*j, 2*xC ); 
					glTexCoord2f( (8.0f/NUM_QUAD) + (8.0f/NUM_QUAD)*i, (2.0f/1) + (2.0f/1)*j );	glVertex3i( (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*i, (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*j, 2*xC ); 
					glTexCoord2f( (8.0f/NUM_QUAD) + (8.0f/NUM_QUAD)*i, (2.0f/1)*j );			glVertex3i( (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*i, (2*xC/NUM_QUAD)*j 		  			, 2*xC );
				glEnd();
			}
		}
		// parte encostada à janela
		for(int i = 10; i< 13; i++){
			for (int j = 0; j< 6; j++){
				glBegin(GL_QUADS);
					glTexCoord2f( (8.0f/30)*i, (2.0f/6)*j );						glVertex3i( (2*xC/30)*i   		   , (2*xC/30)*j 		   	, 2*xC ); 
					glTexCoord2f( (8.0f/30)*i, (2.0f/6) + (2.0f/6)*j );				glVertex3i( (2*xC/30)*i			   , (2*xC/30) + (2*xC/30)*j, 2*xC ); 
					glTexCoord2f( (8.0f/30) + (8.0f/30)*i, (2.0f/6) + (2.0f/6)*j );	glVertex3i( (2*xC/30) + (2*xC/30)*i, (2*xC/30) + (2*xC/30)*j, 2*xC ); 
					glTexCoord2f( (8.0f/30) + (8.0f/30)*i, (2.0f/6)*j );			glVertex3i( (2*xC/30) + (2*xC/30)*i, (2*xC/30)*j 		    , 2*xC );
				glEnd();
			}
		}
		
		// parte de baixo da janela
		for(int i = 13; i< 17; i++){
			for (int j = 0; j< 5; j++){
				glBegin(GL_QUADS);
					glTexCoord2f( (8.0f/30)*i			 , (2.0f/6)*j			  );	glVertex3i( (2*xC/30)*i   		   , (2*xC/30)*j 		   	, 2*xC ); 
					glTexCoord2f( (8.0f/30)*i			 , (2.0f/6) + (2.0f/6)*j);	glVertex3i( (2*xC/30)*i			   , (2*xC/30) + (2*xC/30)*j, 2*xC ); 
					glTexCoord2f( (8.0f/30) + (8.0f/30)*i, (2.0f/6) + (2.0f/6)*j);	glVertex3i( (2*xC/30) + (2*xC/30)*i, (2*xC/30) + (2*xC/30)*j, 2*xC ); 
					glTexCoord2f( (8.0f/30) + (8.0f/30)*i, (2.0f/6)*j			  );	glVertex3i( (2*xC/30) + (2*xC/30)*i, (2*xC/30)*j 		    , 2*xC );
				glEnd();
			}
		}
		
		for(int i = 17; i< 20; i++){
				for (int j = 0; j< 6; j++){
					glBegin(GL_QUADS);
						glTexCoord2f( (8.0f/30)*i			 , (2.0f/6)*j			  );	glVertex3i( (2*xC/30)*i   		   , (2*xC/30)*j 		   	, 2*xC ); 
						glTexCoord2f( (8.0f/30)*i			 , (2.0f/6) + (2.0f/6)*j);	glVertex3i( (2*xC/30)*i			   , (2*xC/30) + (2*xC/30)*j, 2*xC ); 
						glTexCoord2f( (8.0f/30) + (8.0f/30)*i, (2.0f/6) + (2.0f/6)*j);	glVertex3i( (2*xC/30) + (2*xC/30)*i, (2*xC/30) + (2*xC/30)*j, 2*xC ); 
						glTexCoord2f( (8.0f/30) + (8.0f/30)*i, (2.0f/6)*j			  );	glVertex3i( (2*xC/30) + (2*xC/30)*i, (2*xC/30)*j 		    , 2*xC );
					glEnd();
				}
			}
		for(int i = 3; i< 5; i++){
			for (int j = 0; j< 1; j++){
				glBegin(GL_QUADS);
					glTexCoord2f( (8.0f/NUM_QUAD)*i			 , (2.0f/1)*j			  );			glVertex3i( (2*xC/NUM_QUAD)*i   		   , (2*xC/NUM_QUAD)*j 		   	, 2*xC ); 
					glTexCoord2f( (8.0f/NUM_QUAD)*i			 , (2.0f/1) + (2.0f/1)*j);				glVertex3i( (2*xC/NUM_QUAD)*i			   , (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*j, 2*xC ); 
					glTexCoord2f( (8.0f/NUM_QUAD) + (8.0f/NUM_QUAD)*i, (2.0f/1) + (2.0f/1)*j);		glVertex3i( (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*i, (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*j, 2*xC ); 
					glTexCoord2f( (8.0f/NUM_QUAD) + (8.0f/NUM_QUAD)*i, (2.0f/1)*j			  );	glVertex3i( (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*i, (2*xC/NUM_QUAD)*j 		    , 2*xC );
				glEnd();
			}
		}


		glDisable(GL_TEXTURE_2D);

	// desenho da parte de cima dos azuleijos

		//glDisable(GL_LIGHTING);
		glDisable(GL_TEXTURE_2D);
		glColor4f(COR_PAREDE);

		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Parede x=0
		for(int i = 0; i< NUM_QUAD; i++){
			for (int j = 1; j< 3; j++){
				glBegin(GL_QUADS);
					glVertex3i( 0, (2*xC/NUM_QUAD)*j 		   	  , (2*xC/NUM_QUAD)*i   		    ); 
					glVertex3i( 0, (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*j, (2*xC/NUM_QUAD)*i   		    ); 
					glVertex3i( 0, (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*j, (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*i ); 
					glVertex3i( 0, (2*xC/NUM_QUAD)*j 		      , (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*i );
				glEnd();
			}
		}

		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Parede z=0
		
		for(int i = 0; i< NUM_QUAD; i++){
			for (int j = 1; j< 3; j++){
				glBegin(GL_QUADS);
					glVertex3i( (2*xC/NUM_QUAD)*i			   , (2*xC/NUM_QUAD)*j  		    , 0	); 
					glVertex3i( (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*i, (2*xC/NUM_QUAD)*j   		    , 0 );
					glVertex3i( (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*i, (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*j, 0 ); 
					glVertex3i( (2*xC/NUM_QUAD)*i            , (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*j, 0	); 
				glEnd();
			}
		}
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Parede x=2*xC
		
		for(int i = 0; i< NUM_QUAD; i++){
			for (int j = 1; j< 3; j++){
				glBegin(GL_QUADS);
					glVertex3i( 2*xC, (2*xC/NUM_QUAD)*j, (2*xC/NUM_QUAD)*i ); 
					glVertex3i( 2*xC, (2*xC/NUM_QUAD)*j, (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*i );
					glVertex3i( 2*xC, (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*j, (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*i ); 
					glVertex3i( 2*xC, (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*j, (2*xC/NUM_QUAD)*i ); 
				glEnd();
			}
		}

		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Parede z=2*xC
		// desenho das varias partes da parede. Comeca pela parte antes da janela
		// desenha a parte por cima
		// desenha a restante parte da janela
		for(int i = 0; i< 2; i++){
			for (int j = 1; j< 3; j++){
				glBegin(GL_QUADS);
					glVertex3i( (2*xC/NUM_QUAD)*i   		       , (2*xC/NUM_QUAD)*j 		 			, 2*xC ); 
					glVertex3i( (2*xC/NUM_QUAD)*i			   	   , (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*j, 2*xC ); 
					glVertex3i( (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*i, (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*j, 2*xC ); 
					glVertex3i( (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*i, (2*xC/NUM_QUAD)*j 		  			, 2*xC );
				glEnd();
			}
		}
		// parte encostada à janela
		for(int i = 10; i< 13; i++){
			for (int j = 6; j< 15; j++){
				glBegin(GL_QUADS);
					glVertex3i( (2*xC/30)*i   		   , (2*xC/30)*j 		   	, 2*xC ); 
					glVertex3i( (2*xC/30)*i			   , (2*xC/30) + (2*xC/30)*j, 2*xC ); 
					glVertex3i( (2*xC/30) + (2*xC/30)*i, (2*xC/30) + (2*xC/30)*j, 2*xC ); 
					glVertex3i( (2*xC/30) + (2*xC/30)*i, (2*xC/30)*j 		    , 2*xC );
				glEnd();
			}
		}
		
		// parte de cima da janela
		for(int i = 13; i< 17; i++){
			for (int j = 13; j< 15; j++){
				glBegin(GL_QUADS);
					glVertex3i( (2*xC/30)*i   		   , (2*xC/30)*j 		   	, 2*xC ); 
					glVertex3i( (2*xC/30)*i			   , (2*xC/30) + (2*xC/30)*j, 2*xC ); 
					glVertex3i( (2*xC/30) + (2*xC/30)*i, (2*xC/30) + (2*xC/30)*j, 2*xC ); 
					glVertex3i( (2*xC/30) + (2*xC/30)*i, (2*xC/30)*j 		    , 2*xC );
				glEnd();
			}
		}
		
		for(int i = 17; i< 20; i++){
			for (int j = 6; j< 15; j++){
				glBegin(GL_QUADS);
					glVertex3i( (2*xC/30)*i   		   , (2*xC/30)*j 		   	, 2*xC ); 
					glVertex3i( (2*xC/30)*i			   , (2*xC/30) + (2*xC/30)*j, 2*xC ); 
					glVertex3i( (2*xC/30) + (2*xC/30)*i, (2*xC/30) + (2*xC/30)*j, 2*xC ); 
					glVertex3i( (2*xC/30) + (2*xC/30)*i, (2*xC/30)*j 		    , 2*xC );
				glEnd();
			}
		}

		for(int i = 3; i< 5; i++){
			for (int j = 1; j< 3; j++){
				glBegin(GL_QUADS);
					glVertex3i( (2*xC/NUM_QUAD)*i   		   , (2*xC/NUM_QUAD)*j 		   	, 2*xC ); 
					glVertex3i( (2*xC/NUM_QUAD)*i			   , (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*j, 2*xC ); 
					glVertex3i( (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*i, (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*j, 2*xC ); 
					glVertex3i( (2*xC/NUM_QUAD) + (2*xC/NUM_QUAD)*i, (2*xC/NUM_QUAD)*j 		    , 2*xC );
				glEnd();
			}
		}


	glPopMatrix();
	glColor4fv(previousColor);

}

void drawSphere(GLuint texture){
	float previousColor[4];

	glGetFloatv(GL_CURRENT_COLOR,  previousColor);
	
	// Parte do movimento
	// está algo arcaico.
	// parte inicial segue a equacao do movimento até à colisao com a lata 
	if (keyPressed && !Colision)
	{
		//printf("STATE 1\n");
		//printf("x_bola %.2f, y_bola = %.2f\n", x_bola, y_bola);
		time+=0.01f;
		x_bola = ballMotion(x_inicial, time, velocity);
		y_bola = ballMotion(y_inicial, time, -velocity/50);
		//printf("\t\t\t%d\n",  (x <= previous_x || x >= 17.0f));
		if ( x_bola >= 17.5f || y_bola <= 4.2f)
		{
			Colision = 1;
			time = 0;
			x_inicial = x_bola;
			y_inicial = y_bola;
		}
			
	}
	// neste momento a bola já colidiu com a lata 
	// enquanto a lata nao estiver "deitada" faz a sua rotacao progressivamente ( para a proximo renderizacao )
	else if (keyPressed && Colision && -1*rotates[POS_ANGULO_LATA] < 90.0f) // bola já bateu na lata
	{
		//printf("STATE 2\n");
		//printf("LATA:\n\trotates[POS_ANGULO_LATA] = %.2f\n", rotates[POS_ANGULO_LATA]);
		//printf("\ttranslates[POS_X_LATA] = %.2f, translates[POS_Y_LATA] = %.2f\n", translates[POS_X_LATA], translates[POS_Y_LATA]);
		rotates[POS_ANGULO_LATA]-=5.0f;
		translates[POS_X_LATA]+=0.05f;
		translates[POS_Y_LATA]-=0.008f;
		
		if ( translates[POS_X_LATA]>=20.5f || translates[POS_Y_LATA] <= 4.0f)
		{
			keyPressed = 0;
			Colision = 0;
			velocity = 20.0f;
		}
		
	}
	// caso em que já colidiu com a lata ( visto que rotates[pos_angulo_lata] !=0 )
	// esta parte é para o movimento final da bola. Enquanto a velocidade nao chegar a zero
	// calcula a posicao da bola em x e y. [ passar para uma equacao de movimento para que este seja mais natural ]
	if ( velocity > 0.0f && rotates[POS_ANGULO_LATA] != 0.0f){
		x_bola -= 0.0045f * velocity;
		//			a tocar em cima da mesa 		  a tocar no chao
		if ( ( y_bola <= 4.2f  && x_bola >= 9.0f ) || y_bola <= 0.8f )  
			bounce = 1.0;												// logo tem que subir
		else if (y_bola > MAX_ALT)					
		{																// logo tem que descer e já nao chega a MAX_ALT novamente
																		// para dar algum realismo
			bounce = -1.0;
			MAX_ALT -= 0.225f;
		}
		
		y_bola = y_bola  + bounce * 0.008f * velocity;
		

		velocity -= 0.27f;
		//printf("bounce = %.1f\n", bounce);
		//printf("x_bola %.2f, y_bola = %.2f\n", x_bola, y_bola);
		//printf("velocity = %.2f\n", velocity);
	}
	if ( abs(rotates[POS_ANGULO_LATA]) == 90.0f && CAN_NOT_EMPTY){
		drawCone = true;
		time++;
		hCone+=0.004;
		radiusCone+=0.0001f;
		if ( time == 1000 ){
			CAN_NOT_EMPTY = false;
		}

		//printf("drawing flow\n");
		drawParticleSystemFlow(texture, 21.0f, 4.0f, 15.0f, 0.1f+(float)time/1000 );
	}

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_LIGHTING);
	glColor4f(COKECOLOR);
	if ( drawCone ){
		glDisable(GL_CULL_FACE);

		glPushMatrix();
			glTranslatef(21.5f, 0.1f+(float)time/1000, 15.0f);	// posicao copo
			glRotatef(90.0f, 1,0,0);
			
			glutSolidCone(radiusCone, hCone, 16, 16);
		glPopMatrix(); 

		drawConeBase(radiusCone, 0.1f+(float)time/1000);
		drawParticleSystemGlass(texture, 21.5f, 0.1f+(float)time/1000, 15.0f, radiusCone );

		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);

		
	}
	glDisable(GL_BLEND);
	glEnable(GL_LIGHTING);
	//printf("x = %0.2f\nprevious_x = %0.2f\nTime = %0.2f\n", x, previous_x, time);
	glColor4f(GREEN);

	glPushMatrix();
        glTranslatef(x_bola, y_bola, 15.0f);
        glutSolidSphere(0.4,16,16);
    glPopMatrix(); 

	glColor4fv(previousColor);		// repor cor anterior
	
}

void drawConeBase(float raio, float y){
	GLUquadricObj*  q = gluNewQuadric ( );
	
	glPushMatrix();
		glTranslatef(21.5f, y, 15.0f);
		glRotatef(90.0f, 1.0f,0.0f,0.0f);
		gluDisk(q, 0, raio, 16, 16);
	glPopMatrix();	
	
	gluDeleteQuadric ( q );
}

int rand_lim(int limit) {
/* return a random number between 0 and limit inclusive.
 */
    int divisor = RAND_MAX/(limit+1);
    int retval;
	int i = 0;
	while( i < 4 )
    {
		do { 
			retval = rand() / divisor;
		} while (retval > limit);
		i++;
	}
    return retval;
}

void generateCracks(){
	cracks[ 0] = rand_lim(1);			// numCracks em x = 0
	cracks[ 1] = rand_lim(15)+5;		// cord y do crack em x = 0
	cracks[ 2] = rand_lim(30)+5;		// cord z do crack em x = 0
	cracks[ 3] = rand_lim(1);			// numCracks em x = 2*xC
	cracks[ 4] = rand_lim(15)+5;		// cord y do crack em x = 2*xC
	cracks[ 5] = rand_lim(30)+5;		// cord z do crack em x = 2*xC
	cracks[ 6] = rand_lim(1);			// numCracks em z = 0
	cracks[ 7] = rand_lim(30)+5;		// cord x do crack em z = 0
	cracks[ 8] = rand_lim(15)+5;		// cord y do crack em z = 0
	cracks[ 9] = rand_lim(1);			// numCracks em z = 2*xC
	cracks[10] = rand_lim(30)+5;		// cord x do crack em z = 2*xC
	cracks[11] = rand_lim(15)+5;		// cord y do crack em x = 2*xC
	
}
void drawCracksInWalls(GLuint *texture, int xC){			// se encontrarmos um bom gerador de nums serão x cracks aleatorios em cada parede
	int numCracks, x,y,z;

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture[CRACK_TEXTURE]);
	
	// x = 0
	numCracks = cracks[0];
	y = cracks[1];
	z = cracks[2];

	numCracks = 1;
	y = 13;
	z = 14;
	//printf( "X = 0:\n\tnumCracks = %d\n\ty = %d\n\tz = %d\n", numCracks, y,z);
	if(numCracks){
		/*
		for(int i = 0; i< 4; i++){
			for (int j = 0; j< 4; j++){
				glBegin(GL_QUADS);
					glTexCoord2f( (1.0f/4)*i		   , (1.0f/4)*j		  		);	glVertex3f( 0, (y/4)*j 		  , (z/4)*i   		); 
					glTexCoord2f( (1.0f/4)*i		   , (1.0f/4) + (1.0f/4)*j	);	glVertex3f( 0, (y/4) + (y/4)*j, (z/4)*i   		); 
					glTexCoord2f( (1.0f/4) + (1.0f/4)*i, (1.0f/4) + (1.0f/4)*j	);	glVertex3f( 0, (y/4) + (y/4)*j, (z/4) + (z/4)*i ); 
					glTexCoord2f( (1.0f/4) + (1.0f/4)*i, (1.0f/4)*j				);	glVertex3f( 0, (y/4)*j 		  , (z/4) + (z/4)*i );
				glEnd();
			}
		}*/
		glBegin(GL_QUADS);
			glTexCoord2f( 0.0f, 0.0f );	glVertex3f( 0+0.01, y+0, z+0 ); 
			glTexCoord2f( 0.0f, 1.0f );	glVertex3f( 0+0.01, y+5, z+0 ); 
			glTexCoord2f( 1.0f, 1.0f );	glVertex3f( 0+0.01, y+5, z+5 ); 
			glTexCoord2f( 1.0f, 0.0f );	glVertex3f( 0+0.01, y+0, z+5 );
		glEnd();
		
	}
	// x = 30
	numCracks = cracks[3];
	y = cracks[4];
	z = cracks[5];

	numCracks = 1;
	y = 2;
	z = 24;
	//printf( "X = 30:\n\tnumCracks = %d\n\ty = %d\n\tz = %d\n", numCracks, y,z);
	if(numCracks){
		
		/*for(int i = 0; i< 4; i++){
			for (int j = 0; j< 4; j++){
				glBegin(GL_QUADS);
					glTexCoord2f( (1.0f/4)*i		   , (1.0f/4)*j		  		);	glVertex3f( 30, (y/4)*j 		  , (z/4)*i   	  ); 
					glTexCoord2f( (1.0f/4)*i		   , (1.0f/4) + (1.0f/4)*j	);	glVertex3f( 30, (y/4) + (y/4)*j , (z/4)*i   	  ); 
					glTexCoord2f( (1.0f/4) + (1.0f/4)*i, (1.0f/4) + (1.0f/4)*j	);	glVertex3f( 30, (y/4) + (y/4)*j , (z/4) + (z/4)*i ); 
					glTexCoord2f( (1.0f/4) + (1.0f/4)*i, (1.0f/4)*j				);	glVertex3f( 30, (y/4)*j 		  , (z/4) + (z/4)*i );
				glEnd();
			}
		}*/
		glBegin(GL_QUADS);
			glTexCoord2f( 0.0f, 0.0f);	glVertex3f( 2*xC-0.01, y+0, z+0); 
			glTexCoord2f( 1.0f, 0.0f);	glVertex3f( 2*xC-0.01, y+0, z+5); 
			glTexCoord2f( 1.0f, 1.0f);	glVertex3f( 2*xC-0.01, y+5, z+5); 
			glTexCoord2f( 0.0f, 1.0f);	glVertex3f( 2*xC-0.01, y+5, z+0); 
		glEnd();
	}
	
	// z = 0
	numCracks = cracks[6];
	x = cracks[7];
	y = cracks[8];

	numCracks = 1;
	x = 16;
	y = 10;
	//printf( "Z = 0:\n\tnumCracks = %d\n\tx = %d\n\ty = %d\n", numCracks, x,y);
	if(numCracks){
		/*for(int i = 0; i< 4; i++){
			for (int j = 0; j< 4; j++){
				glBegin(GL_QUADS);
					glTexCoord2f( (1.0f/4)*i		   , (1.0f/4)*j		  		);	glVertex3i( (x/4)*i		   , (y/4)*j  		, 0	); 
					glTexCoord2f( (1.0f/4)*i		   , (1.0f/4) + (1.0f/4)*j	);	glVertex3i( (x/4) + (x/4)*i, (y/4)*j   		, 0 );
					glTexCoord2f( (1.0f/4) + (1.0f/4)*i, (1.0f/4) + (1.0f/4)*j	);	glVertex3i( (x/4) + (x/4)*i, (y/4) + (y/4)*j, 0 ); 
					glTexCoord2f( (1.0f/4) + (1.0f/4)*i, (1.0f/4)*j				);	glVertex3i( (x/4)*i        , (y/4) + (y/4)*j, 0	); 
				glEnd();
			}
		}*/
		glBegin(GL_QUADS);
			glTexCoord2f( 0.0f, 0.0f);	glVertex3f( x+0, y+0, 0+0.01); 
			glTexCoord2f( 1.0f, 0.0f);	glVertex3f( x+5, y+0, 0+0.01); 
			glTexCoord2f( 1.0f, 1.0f);	glVertex3f( x+5, y+5, 0+0.01); 
			glTexCoord2f( 0.0f, 1.0f);	glVertex3f( x+0, y+5, 0+0.01); 
		glEnd();
	}
	// z = 30
	numCracks = cracks[9];
	x = cracks[10];
	y = cracks[11];

	numCracks = 1;
	x = 12;
	y = 0;
	//printf( "Z = 30:\n\tnumCracks = %d\n\tx = %d\n\ty = %d\n", numCracks, x,y);
	if(numCracks){
		
		/*if ( !( ( x > 13 && x < 17 ) && ( y >5 && y< 13 ) ) ) // se nao é a posicao da janela
		{
			for(int i = 0; i< 4; i++){
				for (int j = 0; j< 4; j++){
					glBegin(GL_QUADS);
						glTexCoord2f( (1.0f/4)*i		   , (1.0f/4)*j		  		);	glVertex3i( (x/4)*i		   , (y/4)*j  		, 30 ); 
						glTexCoord2f( (1.0f/4)*i		   , (1.0f/4) + (1.0f/4)*j	);	glVertex3i( (x/4) + (x/4)*i, (y/4)*j   		, 30 );
						glTexCoord2f( (1.0f/4) + (1.0f/4)*i, (1.0f/4) + (1.0f/4)*j	);	glVertex3i( (x/4) + (x/4)*i, (y/4) + (y/4)*j, 30 ); 
						glTexCoord2f( (1.0f/4) + (1.0f/4)*i, (1.0f/4)*j				);	glVertex3i( (x/4)*i        , (y/4) + (y/4)*j, 30 ); 
					glEnd();
				}
			}
		}*/
		glBegin(GL_QUADS);
			glTexCoord2f( 0.0f, 0.0f);	glVertex3f( x+0, y+0, 2*xC-0.01); 
			glTexCoord2f( 0.0f, 1.0f);	glVertex3f( x+0, y+5, 2*xC-0.01); 
			glTexCoord2f( 1.0f, 1.0f);	glVertex3f( x+5, y+5, 2*xC-0.01); 
			glTexCoord2f( 1.0f, 0.0f);	glVertex3f( x+5, y+0, 2*xC-0.01); 
		glEnd();
	}

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
}

void resetScene(){
	keyPressed = 0;
	Colision = 0;
	time = 0.0f;
	velocity = 40.0f;
	bounce = -1.0;
	MAX_ALT = 7.0f;
	CAN_NOT_EMPTY = true;
	drawCone = false;
	
	radiusCone = 0.3f;
	hCone = 0.1f;

	timeParticle=0;

	// RESET LATA 
	rotates[0] = 0.0f;	
	translates[0] = 19.5f;
	translates[1] = 4.3f;
	translates[2] = 15.0f;
	
	// RESET BOLA
	x_bola = 10.0f;
	x_inicial = 10.0f;
	y_bola = 4.2f;
	y_inicial = 5.5f;	

	initParticles();
}