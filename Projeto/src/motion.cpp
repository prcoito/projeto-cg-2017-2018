float ballMotion(float initial_point, float time, float velocity){
    return initial_point + velocity*time + ((-9.8*time*time) / 2);
}