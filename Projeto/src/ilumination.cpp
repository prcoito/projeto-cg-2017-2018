#include <stdio.h>
#include <stdlib.h>
#include <GL/glut.h>

GLfloat luzGlobalCor[4] = {0.0, 0.0, 0.0, 0.0}; 

void initLigths(void) {

    //…………………………………………………………………………………………………………………………………………… Ambiente
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, luzGlobalCor);
	
   //…………………………………………………………………………………………………………………………………………… Tecto
	GLfloat localCor[4] 	= { 0.956f, 1.0f, 0.980f , 1.0f }; 
	GLfloat localCorDif[4] 	= { 0.4f, 0.4f, 0.4f, 1.0f };
	GLfloat lightPos[4] 	= { 15.0f, 14.0f, 15.0f, 1.0f };
	GLfloat lightdir[3] 	= { 0.0f , -1.0f , 0.0f };
	GLfloat localAttCon 	= 1.0f;
	GLfloat localAttLin 	= 4.00f;
	GLfloat localAttQua 	= 1.00f;
   
	glLightfv(GL_LIGHT0, GL_POSITION,      			lightPos	);   
	glLightfv(GL_LIGHT0, GL_AMBIENT	,       		localCor 	);   
	glLightfv(GL_LIGHT0, GL_DIFFUSE	,       		localCorDif );   
	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION		, 	lightdir	);     
	glLightf (GL_LIGHT0, GL_CONSTANT_ATTENUATION, 	localAttCon	);
	glLightf (GL_LIGHT0, GL_LINEAR_ATTENUATION	,   localAttLin	);         
	glLightf (GL_LIGHT0, GL_QUADRATIC_ATTENUATION,	localAttQua	); 


    glEnable(GL_LIGHTING);  
	glEnable(GL_LIGHT0);   

}