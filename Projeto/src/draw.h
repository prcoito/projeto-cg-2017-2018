#include <GL/glut.h>
#include "loaderModels.h"

void drawScene(GLuint *texture);
void draw3DModels(Objeto *modelo, GLuint* texture);
void draw3DModel(Objeto modelo, GLuint texture, float x, float y, float z, 
												// scale sx, sy, sz
												float sx, float sy, float sz, 
												// rotate angle, rx, ry, rz
												float angle, float rx, float ry, float rz,
												// repete texture em x e y
												float repeat_x, float repeat_y);
extern int keyPressed;
extern float x_bola;

void generateCracks();

void resetScene();